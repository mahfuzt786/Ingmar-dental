<?php

// require_once('new_rules_region.php');


$TOOTH_NUMBERS_ISO = array(
    18, 17, 16, 15, 14, 13, 12, 11,
    21, 22, 23, 24, 25, 26, 27, 28,
    38, 37, 36, 35, 34, 33, 32, 31,
    41, 42, 43, 44, 45, 46, 47, 48
);

$front_end = array(18 => 'f', 17 => 'f', 16 => 'f', 15 => 'f', 14 => 'f', 13 => 'f', 12 => 'f', 11 => 'f',
    21 => 'f', 22 => 'f', 23 => 'f', 24 => 'f', 25 => 'f', 26 => 'f', 27 => 'f', 28 => 'f',
    38 => 'f', 37 => 'f', 36 => '', 35 => '', 34 => '', 33 => '', 32 => '', 31 => '',
    41 => '', 42 => '', 43 => '', 44 => 'f', 45 => 'ww', 46 => 'ww', 47 => 'f', 48 => 'f');


$teeth_with_status                  = [];
$teeth_region_eveluate              = [];
$teeth_subsidy_eveluate             = [];
$teeth_subsidy_eveluate_upper       = [];
$teeth_subsidy_eveluate_2x          = [];
$is_optional_3x_1x                  = FALSE;
$teeth_inter_dental_gaps            = [];
$is_gap_closure                     = 'N';
$is_gap_closure_arr                 = [];
$bs_separate_arr                    = [];
$is_optional_3x_2x                  = FALSE;
$check_region_2_7                   = [];

// print_r($front_end);
// main_input($front_end);
function main_input($schema) {

    global $teeth_with_status, $teeth_region_eveluate, $teeth_subsidy_eveluate, 
            $teeth_subsidy_eveluate_upper, $is_gap_closure, $is_gap_closure_arr, 
            $TOOTH_NUMBERS_ISO, $is_optional_3x_2x, $teeth_subsidy_eveluate_2x,
            $is_optional_3x_1x, $check_region_2_7;

    // data validation
    if (! is_array($schema)) {
        echo json_encode(array("message" => "Error Processing as data parameter as a array."));
        return;
    }

    if (sizeof($schema) !== 32) {
        // throw new Exception("Each one of the 32 teeth should be represented in the tuple");
        echo json_encode(array("message" => "Each one of the 32 teeth should be represented in the tuple."));
        return;
    }

    //refine the schema for gap closure
    $schema_old = ($schema);
    $schema = gap_closure($schema);
    $schema = bs_TBR($schema);
    $schema = sw_TBR($schema);
    $schema_new = $schema;

    // var_dump($schema);

    $chunks = array_chunk($schema, ceil(count($schema) / 2), true);

    for ($in=0; $in<count($chunks); $in++)
    {
        $schema = $chunks[$in];

        $teeth_subsidy_eveluate_upper = $teeth_subsidy_eveluate;
        $teeth_subsidy_eveluate = [];

        // Check for teeth nos with a status
        foreach ($schema as $tooth_number => $status) {
            // if($status !== '') {
            if(to_be_treated($status)
                OR to_be_replaced($status, $tooth_number, $schema)
                OR to_be_sw($status)
                OR $status == 'b'
            ) {
                array_push($teeth_with_status, intval($tooth_number));
            }
        }

        // var_dump($teeth_with_status);

        is_whole_mouth($teeth_with_status);
        is_upper_jaw($teeth_with_status);
        is_upper_jaw_left_end($schema);
        is_upper_jaw_right_end($schema);
        is_upper_jaw_front($teeth_with_status);
        is_mandible($teeth_with_status);
        is_mandible_left_end($schema);
        is_mandible_right_end($schema);
        is_mandible_front($teeth_with_status);
        is_X7_X8($schema);
        atleastOneInPostRegion($schema);
        inAnteriorRegion($schema);

        /** Execute the rules **/
        //4.x
        Exact16ToBeReplacedTeeth_upper($schema);
        Exact16ToBeReplacedTeeth_mandible($schema);
        Between13And15ToBeReplacedTeeth_upper($schema);
        Between13And15ToBeReplacedTeeth_mandible($schema);

        //3.x
        Between5And12ToBeReplacedTeeth_upper($schema);
        Between5And12ToBeReplacedTeeth_mandible($schema);
        X7_X8_check($schema);
        X7_X8_3x_2x($schema);
        UnilateralFreeEndToBeReplacedTeethAtLeast1_upper($schema);
        UnilateralFreeEndToBeReplacedTeethAtLeast1_mandible($schema);
        BilateralFreeEnd_upper($schema, $teeth_with_status);
        BilateralFreeEnd_mandible($schema, $teeth_with_status);
        special_ob_f($schema);
        special_case_b($schema);

        // 2.x
        BiggestInterdentalGapInFrontRegionExactToBeReplaced4($schema);
        BiggestInterdentalGapExactToBeReplaced3($schema);
        BiggestInterdentalGapExactToBeReplaced2($schema);
        BiggestInterdentalGapExactToBeReplaced1($schema);
        Exact2_3ToBeReplacedTeethInterdentalGapInFrontRegion($schema);
        Exact1ToBeReplacedTeethInterdentalGapInFrontRegion($schema);

        //7.x
        sw_7_2($schema);
        sw_7_1($schema);

        //1.x
        StatusPwInPosteriorRegion($schema);
        StatusPwInFrontRegion($schema);
        ToBeTreatedWithNoAbutmentTeethIncluded($schema);
    }

    $teeth_subsidy_eveluate = array_merge($teeth_subsidy_eveluate_upper, $teeth_subsidy_eveluate);

    // remove dependencies or duplicates
    // sort array in output_order
    // $remove_subsidies = [];

    $output_order = array_column($teeth_subsidy_eveluate, 'output_order');
    array_multisort($output_order, SORT_ASC, $teeth_subsidy_eveluate);

    // $teeth_subsidy_eveluate = array_values(($teeth_subsidy_eveluate));
    // var_dump($teeth_subsidy_eveluate);

    if($is_gap_closure == 'Y') {
        $new_arr_gap = [];
        $teeth_with_status_old = [];

        foreach ($schema_old as $tooth_number => $status) {
            if($status !== '') {
                // array_push($teeth_with_status_old, intval($tooth_number));
            }
        }

        // if(get_condition(end($teeth_with_status_old), $schema_old) !== ')(' 
            ////AND count($is_gap_closure_arr) !== 1
        // )
        {
            foreach($teeth_subsidy_eveluate as $arr_gap) {
                
                for($gap=0; $gap<count($is_gap_closure_arr); $gap++) {

                    // if( ($is_gap_closure_arr[$gap] == '15' OR $is_gap_closure_arr[$gap] == '25' 
                    //         OR $is_gap_closure_arr[$gap] == '34' OR $is_gap_closure_arr[$gap] == '44')
                    //     AND to_be_treated(get_condition($is_gap_closure_arr[$gap], $schema_old))
                    //     AND subsidy_exists(1)
                    // ) {
                    //     array_push($teeth_subsidy_eveluate, 
                    //         // ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                    //         ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                    //     );
                    // }
                    
                    if( ($is_gap_closure_arr[$gap] == '15' OR $is_gap_closure_arr[$gap] == '14'
                            OR $is_gap_closure_arr[$gap] == '13' OR $is_gap_closure_arr[$gap] == '12'
                            OR $is_gap_closure_arr[$gap] == '11'
                            // OR in_array('15', $new_arr_gap) 
                            )
                        // AND (to_be_replaced(get_condition($is_gap_closure_arr[$gap]+1, $schema_old), $is_gap_closure_arr[$gap], $schema_old)
                        //         OR to_be_treated(get_condition($is_gap_closure_arr[$gap]+1, $schema_old)))
                        AND subsidy_exists(1)
                        AND in_array(16, subsidy_exists_region(1.1))
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            // ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> "16", "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                        );
                    }

                    if( ($is_gap_closure_arr[$gap] == '25' OR $is_gap_closure_arr[$gap] == '24'
                            OR $is_gap_closure_arr[$gap] == '23' OR $is_gap_closure_arr[$gap] == '22'
                            OR $is_gap_closure_arr[$gap] == '21'
                            // OR in_array('25', $new_arr_gap)  
                            )
                        // AND (to_be_replaced(get_condition($is_gap_closure_arr[$gap]+1, $schema_old), $is_gap_closure_arr[$gap], $schema_old)
                        //         OR to_be_treated(get_condition($is_gap_closure_arr[$gap]+1, $schema_old)))
                        AND subsidy_exists(1)
                        AND in_array(26, subsidy_exists_region(1.1))
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            // ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> "26", "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                        );
                    }

                    if( ($is_gap_closure_arr[$gap] == '34' OR $is_gap_closure_arr[$gap] == '33'
                        OR $is_gap_closure_arr[$gap] == '32' OR $is_gap_closure_arr[$gap] == '31'
                            // OR in_array('34', $new_arr_gap)  
                            )
                        // AND (to_be_replaced(get_condition($is_gap_closure_arr[$gap]+1, $schema_old), $is_gap_closure_arr[$gap], $schema_old)
                        //         OR to_be_treated(get_condition($is_gap_closure_arr[$gap]+1, $schema_old)))
                        AND subsidy_exists(1)
                        AND in_array(35, subsidy_exists_region(1.1))
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            // ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> "35", "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                        );
                    }

                    if( ($is_gap_closure_arr[$gap] == '44' OR $is_gap_closure_arr[$gap] == '43'
                    OR $is_gap_closure_arr[$gap] == '42' OR $is_gap_closure_arr[$gap] == '41'
                            // OR in_array('44', $new_arr_gap)  
                            )
                        // AND (to_be_replaced(get_condition($is_gap_closure_arr[$gap]+1, $schema_old), $is_gap_closure_arr[$gap], $schema_old)
                        //         OR to_be_treated(get_condition($is_gap_closure_arr[$gap]+1, $schema_old)))
                        AND subsidy_exists(1)
                        AND in_array(45, subsidy_exists_region(1.1))
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            // ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $is_gap_closure_arr[$gap], "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> "45", "quantity"=> "1", "applied_rule"=> "veneering_grants 12 edit"]
                        );
                    }

                    if(get_condition($TOOTH_NUMBERS_ISO[position_schema($is_gap_closure_arr[$gap])+1], $schema_old) == '' ) {
                        for($iter=0; $iter<count($teeth_subsidy_eveluate); $iter++) {
                            // if(strpos($teeth_subsidy_eveluate[$iter]['region'], '-')
                            //     // AND get_condition($TOOTH_NUMBERS_ISO[position_schema(end($teeth_with_status_old))], $schema_old) !== ')('
                            // ) {
                            //     //no changes if )( is in last not before any status
                            //     $is_greater = 0;
                            //     for($bigger=0; $bigger<count($is_gap_closure_arr); $bigger++) {
                            //         // TO fix. another loop required for $teeth_with_status[$zz]
                            //         if(position_schema($teeth_with_status[0]) > position_schema($is_gap_closure_arr[$bigger]))
                            //         {
                            //             $is_greater += 1;
                            //         }
                            //     }

                            //     $teeth_subsidy_eveluate[$iter]['region'] = $TOOTH_NUMBERS_ISO[position_schema($new_arr_gap[0])].'-'.$TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))+$is_greater];

                            //     // if($is_greater > 0
                            //     //     AND count($is_gap_closure_arr) !== 1
                            //     // ) {
                            //     //     $teeth_subsidy_eveluate[$iter]['region'] = $TOOTH_NUMBERS_ISO[position_schema($new_arr_gap[0])+$is_greater].'-'.$TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))+$is_greater];
                            //     // }

                            //     // if($is_greater > 0
                            //     //     AND count($is_gap_closure_arr) == 1
                            //     // ) {
                            //     //     $teeth_subsidy_eveluate[$iter]['region'] = $TOOTH_NUMBERS_ISO[position_schema($new_arr_gap[0])+$is_greater].'-'.$TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))];
                            //     // }
                            // }
                            // else 
                            {
                                if(count($new_arr_gap) > 1) {
                                    if(is_numeric($teeth_subsidy_eveluate[$iter]['region']) AND
                                        to_be_treated(get_condition($teeth_subsidy_eveluate[$iter]['region']+1, $schema_old))
                                    ) {
                                        // $teeth_subsidy_eveluate[$iter]['region'] = $TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))+1];
                                        $teeth_subsidy_eveluate[$iter]['region'] .= ','. $TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))];
                                    }
                                    else {
                                        // // $teeth_subsidy_eveluate[$iter]['region'] .= ','. $TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))+1];
                                        // $teeth_subsidy_eveluate[$iter]['region'] .= ','. $TOOTH_NUMBERS_ISO[position_schema(end($new_arr_gap))+count($is_gap_closure_arr)-1];
                                    }
                                }                                
                            }
                        }
                    }
                }
                
                // removes )( values from output
                $temp_arr_2 =[];
                for($iter=0; $iter<count($teeth_subsidy_eveluate); $iter++) {
                    if(! strpos($teeth_subsidy_eveluate[$iter]['region'], '-') 
                        AND count($new_arr_gap) > 1
                        AND $teeth_subsidy_eveluate[$iter]['region'] == '2.7'
                    ) {
                        $teeth_subsidy_eveluate[$iter]['region'] = array_values(array_unique(explode(',' ,$teeth_subsidy_eveluate[$iter]['region'])));
                        for($z=0; $z<count($teeth_subsidy_eveluate[$iter]['region']); $z++) {
                            if(!in_array($teeth_subsidy_eveluate[$iter]['region'][$z], $is_gap_closure_arr) 
                            ) {

                                array_push($temp_arr_2, $teeth_subsidy_eveluate[$iter]['region'][$z]);
                            }
                        }

                        $temp_arr_2 = array_unique(array_filter($temp_arr_2));

                        $teeth_subsidy_eveluate[$iter]['region'] = implode(',', $temp_arr_2);
                    }
                }
            }
        }
    }

    $opt_region = [];
    $teeth_subsidy_eveluate_2x_opt = [];
    // $region_order_arr_opt = [];
    
    foreach($teeth_subsidy_eveluate_2x as $arr_final_2x) {
        array_push($opt_region, $arr_final_2x['region']);
        // var_dump($opt_region);
        array_push($teeth_subsidy_eveluate_2x_opt, $arr_final_2x);
    }

    foreach($teeth_subsidy_eveluate as $arr_final) {
        if(!startswith($arr_final['subsidy'], '2')
            // AND !startswith($arr_final['subsidy'], '7')
        ) {
            array_push($teeth_subsidy_eveluate_2x_opt, $arr_final);
        }

        if(startswith($arr_final['subsidy'], '2.1')
            OR startswith($arr_final['subsidy'], '2.2')
            OR startswith($arr_final['subsidy'], '2.3')
            OR startswith($arr_final['subsidy'], '2.4')
            OR startswith($arr_final['subsidy'], '2.5')
        ) {
            $temp_divide_opt = explode('-' , $arr_final['region']);

            if( !in_array(get_jaw($temp_divide_opt[0]), $opt_region)
            ) {
                array_push($teeth_subsidy_eveluate_2x_opt, $arr_final);

                // if(startswith($arr_final['subsidy'], '2.7') )
                // {
                //     array_push($teeth_subsidy_eveluate_2x_opt, $arr_final);
                // }

            }
        }
    }

    $output_order_opt = array_column($teeth_subsidy_eveluate_2x_opt, 'output_order');
    array_multisort($output_order_opt, SORT_ASC, $teeth_subsidy_eveluate_2x_opt);
    $teeth_subsidy_eveluate_2x = $teeth_subsidy_eveluate_2x_opt;

    if (array_is_sub($teeth_subsidy_eveluate, $teeth_subsidy_eveluate_2x_opt)
        AND $teeth_subsidy_eveluate_2x_opt !== $teeth_subsidy_eveluate
    ) {
        // echo "no_opt is a subset of opt.";
        $teeth_subsidy_eveluate = [];
    } 
    // else {
        // echo "no_opt is NOT a subset of opt.";
    // }

    $final_2x = [];

    $final = final_subsidy_eveluate($teeth_subsidy_eveluate);
    if($is_optional_3x_2x) {
        $final_2x = final_subsidy_eveluate($teeth_subsidy_eveluate_2x);
    }

    // var_dump($final);
    // RV Implementation
    
    $rv_implement = rv_implement($final, $teeth_with_status, $schema_old, $schema_new);
    if($is_optional_3x_2x) {
        $rv_implement_2x = rv_implement($final_2x, $teeth_with_status, $schema_old, $schema_new);
    }

    // veneering grant calculation fixes
    $count_ven_ok_uk = 0; //For 2.7 in 7.2s

    foreach($final as $arr_final => $values) {
        if( $arr_final == '2.7'
        ) {
            $check_region_2_7   = array_values(array_unique(explode(',', $final[$arr_final]['region'])));

            // $count_ven_ok_uk = $final[$arr_final]['quantity']; //For 2.7 in 7.2s
            $count_ven_ok_uk = count($check_region_2_7);

            // var_dump($count_ven_ok_uk);
            // var_dump($check_region_2_7);
        }
    }
    foreach($final as $arr_final => $values) {
        $region_order_arr = [];

        if( ($arr_final == '1.3'
            OR $arr_final == '2.7'
            OR $arr_final == '4.6'
            OR $arr_final == '4.7'
            OR $arr_final == '1.1'
            OR $arr_final == '1.2'
            OR $arr_final == '3.2'
            OR $arr_final == '7.1'
            OR $arr_final == '7.2')
        ) {
            $final[$arr_final]['region'] = array_values(array_unique(explode(',', $final[$arr_final]['region'])));
            
            // Display the output in order start
            for($az=0; $az<count($final[$arr_final]['region']); $az++)
            {
                array_push($region_order_arr, position_schema($final[$arr_final]['region'][$az]));
            }
            sort($region_order_arr);

            for($ax=0; $ax<count($region_order_arr); $ax++)
            {
                $region_order_arr[$ax] = $TOOTH_NUMBERS_ISO[$region_order_arr[$ax]];
            }

            $final[$arr_final]['region'] = $region_order_arr;
            // Display the output in order end

            $final[$arr_final]['quantity'] = count($final[$arr_final]['region']);
            $final[$arr_final]['region'] = implode(",", $final[$arr_final]['region']);
        }

        if( $arr_final == '2.1'
            OR $arr_final == '2.2'
            OR $arr_final == '2.3'
        ) {
            $final[$arr_final]['region'] = array_values(array_unique(explode(',', $final[$arr_final]['region'])));
            
            $region_order_arr_temp = [];
            $region_order_arr_new = [];
        
            // Display the output in order start
            for($az=0; $az<count($final[$arr_final]['region']); $az++)
            {
                $temp_divide = (explode('-' ,$final[$arr_final]['region'][$az]));
                // array_push($region_order_arr, position_schema($final[$arr_final]['region'][$az]));
                array_push($region_order_arr, position_schema($temp_divide[0]));
            }
            
            $region_order_arr_temp = $region_order_arr;
            sort($region_order_arr);

            for($ax=0; $ax<count($region_order_arr); $ax++)
            {
                $position_old = array_search($region_order_arr_temp[$ax], $region_order_arr);
                $region_order_arr_new[$ax] = $final[$arr_final]['region'][$position_old];//$TOOTH_NUMBERS_ISO[$region_order_arr[$ax]];
            }

            $final[$arr_final]['region'] = $region_order_arr_new;

            // Display the output in order end
            $final[$arr_final]['quantity'] = count($final[$arr_final]['region']);
            $final[$arr_final]['region'] = implode(",", $final[$arr_final]['region']);
        }

        if( $arr_final == '7.2'
        ) {
            $check_region       = array_values(array_unique(explode(',', $final[$arr_final]['region'])));
            $count_ok           = 0;
            $count_uk           = 0;
            $count_ok_uk        = 0;
            $count_ven_ok       = 0; // For 2.7 in 7.2s
            $count_ven_uk       = 0; // For 2.7 in 7.2s
            // $count_ven_ok_uk    = 0; // For 2.7 in 7.2s
            $arr_72_27          = []; // find intersection

            for($ch=0; $ch<count($check_region); $ch++
            ) {
                if(get_jaw($check_region[$ch]) == 'OK'
                ) {
                    $count_ok += 1;
                }

                if(get_jaw($check_region[$ch]) == 'UK'
                ) {
                    $count_uk += 1;
                }

                if(in_array($check_region[$ch], $check_region_2_7)
                ) {
                    array_push($arr_72_27, $check_region[$ch]);
                    
                    if(get_jaw($check_region[$ch]) == 'OK'
                    ) {
                        $count_ven_ok += 1;
                    }

                    if(get_jaw($check_region[$ch]) == 'UK'
                    ) {
                        $count_ven_uk += 1;
                    }
                }
            }

            // MAX quantity 7.2 in a jaw is 4
            $count_ok = ($count_ok < 5) ? $count_ok : 4;
            $count_uk = ($count_uk < 5) ? $count_uk : 4;
            
            $count_ok_uk = $count_ok + $count_uk;
            $final[$arr_final]['quantity'] = $count_ok_uk;
            // $rv_implement = [];

            $count_ven_ok = ($count_ven_ok < 5) ? $count_ven_ok : 4;
            $count_ven_uk = ($count_ven_uk < 5) ? $count_ven_uk : 4;
            $count_ven_ok_uk = $count_ven_ok + $count_ven_uk;

            $result = array_diff($check_region_2_7, $arr_72_27);
            $count_ven_ok_uk = $count_ven_ok_uk + count($result);

            // var_dump($result);
        }
    }

    //2.7 again to fix with max 4 with 7.2
    foreach($final as $arr_final => $values) {
        if( $arr_final == '2.7'
        ) {
            $final[$arr_final]['quantity'] = $count_ven_ok_uk;
        }
    } 

    ksort($rv_implement);
    $final['RV'] = array_values($rv_implement);

    if($is_optional_3x_2x) {

        // veneering grant calculation fixes
        foreach($final_2x as $arr_final_2x => $values_2x) {
            $region_order_arr_2x = [];

            if( ($arr_final_2x == '1.3'
                OR $arr_final_2x == '2.7'
                OR $arr_final_2x == '4.6'
                OR $arr_final_2x == '4.7'
                OR $arr_final_2x == '1.1'
                OR $arr_final_2x == '1.2'
                OR $arr_final_2x == '3.2'
                OR $arr_final_2x == '7.1'
                OR $arr_final_2x == '7.2')
            ) {
                $final_2x[$arr_final_2x]['region'] = array_values(array_unique(explode(',', $final_2x[$arr_final_2x]['region'])));
                
                // Display the output in order start
                for($az_2x=0; $az_2x<count($final_2x[$arr_final_2x]['region']); $az_2x++)
                {
                    array_push($region_order_arr_2x, position_schema($final_2x[$arr_final_2x]['region'][$az_2x]));
                }
                sort($region_order_arr_2x);

                for($ax_2x=0; $ax_2x<count($region_order_arr_2x); $ax_2x++)
                {
                    $region_order_arr_2x[$ax_2x] = $TOOTH_NUMBERS_ISO[$region_order_arr_2x[$ax_2x]];
                }

                $final_2x[$arr_final_2x]['region'] = $region_order_arr_2x;
                // Display the output in order end

                $final_2x[$arr_final_2x]['quantity'] = count($final_2x[$arr_final_2x]['region']);
                $final_2x[$arr_final_2x]['region'] = implode(",", $final_2x[$arr_final_2x]['region']);
            }

            if( $arr_final_2x == '2.1'
                OR $arr_final_2x == '2.2'
                OR $arr_final_2x == '2.3'
            ) {
                $final_2x[$arr_final_2x]['region'] = array_values(array_unique(explode(',', $final_2x[$arr_final_2x]['region'])));
                
                $region_order_arr_temp_2x = [];
                $region_order_arr_new_2x = [];
            
                // Display the output in order start
                for($az_2x=0; $az_2x<count($final_2x[$arr_final_2x]['region']); $az_2x++)
                {
                    $temp_divide_2x = (explode('-' ,$final_2x[$arr_final_2x]['region'][$az_2x]));
                    array_push($region_order_arr_2x, position_schema($temp_divide_2x[0]));
                }
                
                $region_order_arr_temp_2x = $region_order_arr_2x;
                sort($region_order_arr_2x);

                for($ax_2x=0; $ax_2x<count($region_order_arr_2x); $ax_2x++)
                {
                    $position_old_2x = array_search($region_order_arr_temp_2x[$ax_2x], $region_order_arr_2x);
                    $region_order_arr_new_2x[$ax_2x] = $final_2x[$arr_final_2x]['region'][$position_old_2x];//$TOOTH_NUMBERS_ISO[$region_order_arr[$ax]];
                }

                $final_2x[$arr_final_2x]['region'] = $region_order_arr_new_2x;

                // Display the output in order end
                $final_2x[$arr_final_2x]['quantity'] = count($final_2x[$arr_final_2x]['region']);
                $final_2x[$arr_final_2x]['region'] = implode(",", $final_2x[$arr_final_2x]['region']);
            }

            if($arr_final_2x == '7.2'
            ) {
                $final_2x[$arr_final_2x]['quantity'] = ($final_2x[$arr_final_2x]['quantity'] < 5) ? $final_2x[$arr_final_2x]['quantity'] : 4;
                // $rv_implement_2x = [];
            }
        }

        ksort($rv_implement_2x);
        $final_2x['RV'] = array_values($rv_implement_2x);
    }

    $no_final_2x = 'display';
    $final_new = [];

    if(//$no_final_2x == 'display'
        //AND 
        count($final_2x) > 0
    ) {
        if(count($final['RV']) > 0) {
            array_push($final_new, (array_values($final_2x)));

            array_push($final_new, (array_values($final)));
        }
        else {
            $final_new = $final_2x;
        }

        echo json_encode(array_values($final_new));
    }
    else {
        echo json_encode(array_values($final));
    }

    // echo json_encode($teeth_subsidy_eveluate);
}

function final_subsidy_eveluate($teeth_subsidy_eveluate) {
    $final = [];

    foreach($teeth_subsidy_eveluate as $arr) {
        
        $final[$arr['subsidy']]['subsidy'] = $arr['subsidy'];
        
        $final[$arr['subsidy']]['region'] = (isset($final[$arr['subsidy']]['region']) AND ($final[$arr['subsidy']]['region'] !== $arr['region']) ) ? $final[$arr['subsidy']]['region'].','. $arr['region'] : $arr['region'];
        
        $final[$arr['subsidy']]['quantity'] = (isset($final[$arr['subsidy']]['region']) AND ($final[$arr['subsidy']]['region'] !== $arr['region'])) ?  $final[$arr['subsidy']]['quantity']+ $arr['quantity'] : $arr['quantity'];
    }

    return $final;
}

function rv_implement($final, $teeth_with_status, $schema_old, $schema_new) {
    global $TOOTH_NUMBERS_ISO, $is_gap_closure_arr;

    $visible_region = [15,14,13,12,11,21,22,23,24,25,34,33,32,31,41,42,43,44];
    $rv_array = [];
    $rv_add_arr = [];
    $no_h_3_2 = FALSE;

    foreach($final as $arr_final => $values) {
        if($arr_final == '3.2'
        ) {
            $no_h_3_2 = TRUE;
        }
    }

    foreach($final as $arr_final => $values) {
        // if( ($arr_final == '1.3'
        //     OR $arr_final == '2.7'
        //     OR $arr_final == '4.7'
        //     OR $arr_final == '1.1'
        //     OR $arr_final == '1.2')
        // ) 
        {
            $rv_array = array_values(array_unique(explode(',', $final[$arr_final]['region'])));
            
            // Display the output in order start
            for($ax=0; $ax<count($rv_array); $ax++)
            {
                if($arr_final == '1.1'
                ) {
                    if(in_array($rv_array[$ax], $visible_region)) {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : KV';
                        // $rv_add_arr[position_schema($rv_array[$ax])] = isset($rv_add_arr[position_schema($rv_array[$ax])]) ? $rv_add_arr[position_schema($rv_array[$ax])] . $rv_array[$ax].' : KV' : $rv_array[$ax].' : KV';
                    }
                    else {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : K';
                        // $rv_add_arr[position_schema($rv_array[$ax])] = isset($rv_add_arr[position_schema($rv_array[$ax])]) ? $rv_add_arr[position_schema($rv_array[$ax])] . $rv_array[$ax].' : K' : $rv_array[$ax].' : K';
                    }
                }

                if($arr_final == '1.2'
                ) {
                    $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : PK';
                    // $rv_add_arr[position_schema($rv_array[$ax])] = isset($rv_add_arr[position_schema($rv_array[$ax])]) ? $rv_add_arr[position_schema($rv_array[$ax])] . $rv_array[$ax].' : PK' : $rv_array[$ax].' : PK';
                }

                if($arr_final == '1.3'
                ) {
                    if(!in_array($rv_array[$ax], subsidy_exists_region(7.1))
                        AND !in_array($rv_array[$ax], subsidy_exists_region(7.2))
                    ) {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : KV';
                    }
                }

                if($arr_final == '7.1'
                ) {
                    if(in_array($rv_array[$ax], $visible_region)) {
                        if(right($rv_array[$ax], $schema_old)['status'] == ''
                            AND left($rv_array[$ax], $schema_old)['status'] == ''
                        ) {
                            $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : SKV';
                        }
                    }
                    else {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : SK';
                    }
                }

                if($arr_final == '2.1' OR $arr_final == '2.2' 
                    OR $arr_final == '2.3' OR $arr_final == '2.4'
                    OR $arr_final == '2.5'
                ) {
                    $new_arr_rv = [];
                    // $temp_2x = [];
                    $temp_2x = explode('-' , $rv_array[$ax]);

                    // if(count($temp_2x) > 1) {
                    for($az=position_schema($temp_2x[0]); $az<=position_schema(end($temp_2x)); $az++ ) {
                        array_push($new_arr_rv, $TOOTH_NUMBERS_ISO[$az]);
                    }
                    // }

                    $new_arr_rv = array_unique($new_arr_rv);
                    
                    // sort the positions
                    $sort_temp_arr_rv = [];
                    for($rv_arr=0; $rv_arr<count($new_arr_rv); $rv_arr++) {
                        array_push($sort_temp_arr_rv, position_schema($new_arr_rv[$rv_arr]));
                    }
                    sort($sort_temp_arr_rv);

                    $sort_temp_arr_rv_2 = [];
                    for($rv_arr=0; $rv_arr<count($sort_temp_arr_rv); $rv_arr++) {
                        array_push($sort_temp_arr_rv_2, $TOOTH_NUMBERS_ISO[$sort_temp_arr_rv[$rv_arr]]);
                    }
                    $new_arr_rv = $sort_temp_arr_rv_2;

                    // var_dump($new_arr_rv);
                    for($rx=0; $rx<count($new_arr_rv); $rx++)
                    {
                        if(in_array($new_arr_rv[$rx], $visible_region)) {
                            if(in_array($new_arr_rv[$rx], $teeth_with_status) AND
                                (to_be_replaced(get_condition($new_arr_rv[$rx], $schema_old), $new_arr_rv[$rx], $schema_old)
                                    OR get_condition($new_arr_rv[$rx], $schema_old) == 'b'
                                    OR to_be_replaced(get_condition($new_arr_rv[$rx], $schema_new), $new_arr_rv[$rx], $schema_new)
                                )
                            ) {
                                $rv_add_arr[position_schema($new_arr_rv[$rx])] = $new_arr_rv[$rx].' : BV';
                                // $rv_add_arr[position_schema($new_arr_rv[$rx])] = isset($rv_add_arr[position_schema($new_arr_rv[$rx])]) ? $rv_add_arr[position_schema($new_arr_rv[$rx])] : $new_arr_rv[$rx].' : BV';
                            }
                            else {
                                $rv_add_arr[position_schema($new_arr_rv[$rx])] = $new_arr_rv[$rx].' : KV';
                                // $rv_add_arr[position_schema($new_arr_rv[$rx])] = isset($rv_add_arr[position_schema($new_arr_rv[$rx])]) ? $rv_add_arr[position_schema($new_arr_rv[$rx])] : $new_arr_rv[$rx].' : KV';
                            }
                        }
                        else {
                            if(in_array($new_arr_rv[$rx], $teeth_with_status) AND
                                (to_be_replaced(get_condition($new_arr_rv[$rx], $schema_old), $new_arr_rv[$rx], $schema_old)
                                    OR get_condition($new_arr_rv[$rx], $schema_old) == 'b'
                                )
                            ) {
                                $rv_add_arr[position_schema($new_arr_rv[$rx])] = $new_arr_rv[$rx].' : B';
                                // $rv_add_arr[position_schema($new_arr_rv[$rx])] = isset($rv_add_arr[position_schema($new_arr_rv[$rx])]) ? $rv_add_arr[position_schema($new_arr_rv[$rx])] : $new_arr_rv[$rx].' : B';
                            }
                            else {
                                $rv_add_arr[position_schema($new_arr_rv[$rx])] = $new_arr_rv[$rx].' : K';
                                // $rv_add_arr[position_schema($new_arr_rv[$rx])] = isset($rv_add_arr[position_schema($new_arr_rv[$rx])]) ? $rv_add_arr[position_schema($new_arr_rv[$rx])] : $new_arr_rv[$rx].' : K';
                            }
                        }
                    }
                }

                if($arr_final == '2.7'
                ) {
                    if(!in_array($rv_array[$ax], subsidy_exists_region(7.2))
                    ) {
                        if(in_array($rv_array[$ax], $teeth_with_status) AND
                            (to_be_replaced(get_condition($rv_array[$ax], $schema_old), $rv_array[$ax], $schema_old)
                                OR get_condition($rv_array[$ax], $schema_old) == 'b'
                                OR to_be_replaced(get_condition($rv_array[$ax], $schema_new), $rv_array[$ax], $schema_new)
                            )
                        ) {
                            $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : BV';
                        }
                        else {
                            $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : KV';
                        }
                    }
                }

                if($arr_final == '3.1'
                ) {
                    for($xy=0; $xy<count($teeth_with_status); $xy++) {
                        if((!to_be_treated(get_condition($teeth_with_status[$xy], $schema_old))
                            // OR to_be_sw(get_condition($teeth_with_status[$xy], $schema_old))
                            )
                            AND !in_array($teeth_with_status[$xy], [18,28,38,48])
                        ) {
                            $rv_add_arr[position_schema($teeth_with_status[$xy])] = $teeth_with_status[$xy].' : E';
                        }

                        if(to_be_hs(get_condition(left($teeth_with_status[$xy], $schema_old)['tooth'], $schema_old))
                            // AND !in_array(left($teeth_with_status[$xy], $schema_old)['tooth'], [18,38])
                            AND !in_array($teeth_with_status[$xy], [18,28,38,48])
                            AND $no_h_3_2 == FALSE
                        ) {
                            $rv_add_arr[position_schema($teeth_with_status[$xy])-1] = $TOOTH_NUMBERS_ISO[position_schema($teeth_with_status[$xy])-1].' : H';
                        }

                        if(to_be_hs(get_condition(right($teeth_with_status[$xy], $schema_old)['tooth'], $schema_old))
                            // AND !in_array(right($teeth_with_status[$xy], $schema_old)['tooth'], [28,48])
                            AND !in_array($teeth_with_status[$xy], [18,28,38,48])
                            AND $no_h_3_2 == FALSE
                        ) {
                            $rv_add_arr[position_schema($teeth_with_status[$xy])+1] = $TOOTH_NUMBERS_ISO[position_schema($teeth_with_status[$xy])+1].' : H';
                        }
                    }
                }

                if($arr_final == '3.2' AND !in_array($rv_array[$ax], [18,28,38,48])
                ) {
                    if(in_array($rv_array[$ax], $visible_region)) {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : TV';
                        // $rv_add_arr[position_schema($rv_array[$ax])] = isset($rv_add_arr[position_schema($rv_array[$ax])]) ? $rv_add_arr[position_schema($rv_array[$ax])] . $rv_array[$ax].' : TV' : $rv_array[$ax].' : TV';
                    }
                    else {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : T';
                        // $rv_add_arr[position_schema($rv_array[$ax])] = isset($rv_add_arr[position_schema($rv_array[$ax])]) ? $rv_add_arr[position_schema($rv_array[$ax])] . $rv_array[$ax].' : T' : $rv_array[$ax].' : T';
                    }
                }

                if( ($arr_final == '4.2')
                ) {
                    for($xy=1; $xy<15; $xy++) {
                        $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : E';
                        // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : E' : $TOOTH_NUMBERS_ISO[$xy].' : E';
                    }
                }

                if( ($arr_final == '4.4')
                ) {
                    for($xy=16; $xy<32; $xy++) {
                        $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : E';
                        // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : E' : $TOOTH_NUMBERS_ISO[$xy].' : E';
                    }
                }

                if( ($arr_final == '4.1')
                ) {
                    for($xy=1; $xy<15; $xy++) {
                        // var_dump($schema_old[$TOOTH_NUMBERS_ISO[$xy]]);
                        if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : E';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : E' : $TOOTH_NUMBERS_ISO[$xy].' : E';
                        }

                        if(to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                            AND in_array($TOOTH_NUMBERS_ISO[$xy], $visible_region)
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : TV';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : TV' : $TOOTH_NUMBERS_ISO[$xy].' : TV';
                        }

                        if(to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                            AND !in_array($TOOTH_NUMBERS_ISO[$xy], $visible_region)
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : T';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : T' : $TOOTH_NUMBERS_ISO[$xy].' : T';
                        }
                    }
                }

                if( ($arr_final == '4.3')
                ) {
                    for($xy=16; $xy<32; $xy++) {
                        if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : E';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : E' : $TOOTH_NUMBERS_ISO[$xy].' : E';
                        }

                        if(to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                            AND in_array($TOOTH_NUMBERS_ISO[$xy], $visible_region)
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : TV';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : TV' : $TOOTH_NUMBERS_ISO[$xy].' : TV';
                        }

                        if(to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$xy], $schema_old))
                            AND !in_array($TOOTH_NUMBERS_ISO[$xy], $visible_region)
                        ) {
                            $rv_add_arr[$xy] = $TOOTH_NUMBERS_ISO[$xy].' : T';
                            // $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] = isset($rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]]) ? $rv_add_arr[$TOOTH_NUMBERS_ISO[$xy]] . $TOOTH_NUMBERS_ISO[$xy].' : T' : $TOOTH_NUMBERS_ISO[$xy].' : T';
                        }
                    }
                }

                if( ($arr_final == '4.6')
                ) {
                    if(in_array($rv_array[$ax], $visible_region)) {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : TV';
                    }
                    else {
                        $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : T';
                    }
                }

                // if( ($arr_final == '4.7')
                // ) {
                //     $rv_add_arr[position_schema($rv_array[$ax])] = $rv_array[$ax].' : E';
                // }

                if($arr_final == '7.2'
                ) {
                    $rv_add_arr = [];
                }
            }
        }
    }

    // remove Gap Closures from RV array
    foreach($rv_add_arr as $position => $values) {
        if(in_array($TOOTH_NUMBERS_ISO[$position], $is_gap_closure_arr)
        ) {
            unset($rv_add_arr[$position]);
        }
    }

    return $rv_add_arr;
}

function array_diff_assoc_recursive($array1, $array2) {
    $difference=array();
    foreach($array1 as $key => $value) {
        if( is_array($value) ) {
            if( !isset($array2[$key]) || !is_array($array2[$key]) ) {
                $difference[$key] = $value;
            } else {
                $new_diff = array_diff_assoc_recursive($value, $array2[$key]);
                if( !empty($new_diff) )
                    $difference[$key] = $new_diff;
            }
        } else if( !array_key_exists($key,$array2) || $array2[$key] !== $value ) {
            $difference[$key] = $value;
        }
    }
    return $difference;
}

function array_is_sub($needle, $haystack)
{
  foreach ($needle as $n)
  {  
    $matched=false;
    foreach ($haystack as $h)
    {
      if (empty(array_diff_assoc_recursive($n, $h)))
      { 
         $matched=true;
         break;
      }
    }
    if (!$matched) return false;
  }
  return true;
}

function startswith ($string, $startString) { 
    $len = strlen($startString);
    return (substr($string, 0, $len) == $startString);
}

function sub_array_teeth($start, $end, $size) {
    global $TOOTH_NUMBERS_ISO;

    $teeth_region = array_slice($GLOBALS['TOOTH_NUMBERS_ISO'], $start, $size); //ISSUES earlier 'teeth_with_status'
    // $teeth_region = array_slice($GLOBALS['teeth_with_status'], $start, $size);
    
    // echo nl2br("\n");

    if (count($teeth_region) == $size AND end($teeth_region) == $end AND $teeth_region[0] == $TOOTH_NUMBERS_ISO[$start])
        return TRUE;
    else
        return FALSE;
}

function is_whole_mouth($teeth) {
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_selected(18), 48, 32) ) {
        array_push($teeth_region_eveluate, 'whole mouth');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

# Upper jaw
function is_upper_jaw($teeth) {
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_selected(18), 28, 16) ) {
        array_push($teeth_region_eveluate, 'upper_jaw');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function is_upper_jaw_left_end($schema) {
    /*"""
    Represent the end region (three last teeth) for the left side of the mouth.
    """*/
    global $teeth_region_eveluate;

    // array_key_exists(18, $schema) AND 

    if(sub_array_teeth(position_schema(18), 16, 3)
    ) {
        if(array_key_exists(18, $schema) AND to_be_replaced(get_condition(18, $schema), 18, $schema)
            AND array_key_exists(17, $schema) AND to_be_replaced(get_condition(17, $schema), 17, $schema)
            AND array_key_exists(16, $schema) AND to_be_replaced(get_condition(16, $schema), 16, $schema)
        ) {
            array_push($teeth_region_eveluate, 'upper_jaw_left_end');
            return TRUE;
        }
    }
    else {
        return FALSE;
    }
}

function is_upper_jaw_right_end($schema) {
    /*"""
    Represent the end region (three last teeth) for the right side of the mouth.
    """*/
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_schema(26), 28, 3) ) {
        if(array_key_exists(28, $schema) AND to_be_replaced(get_condition(28, $schema), 28, $schema)
            AND array_key_exists(27, $schema) AND to_be_replaced(get_condition(27, $schema), 27, $schema)
            AND array_key_exists(26, $schema) AND to_be_replaced(get_condition(26, $schema), 26, $schema)
        ) {
            array_push($teeth_region_eveluate, 'upper_jaw_right_end');

            return TRUE;
        }
    }
    else {
        return FALSE;
    }
}

function is_upper_jaw_front($teeth) {
    /*"""
    Represent the front (anterior) region (6 front teeth) of the mouth.
    """*/
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_selected(13), 23, 6) ) {
        array_push($teeth_region_eveluate, 'upper_jaw_front');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

# Mandible
function is_mandible($teeth) {
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_selected(38), 48, 16) ) {
        array_push($teeth_region_eveluate, 'mandible');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function is_mandible_left_end($schema) {
    /*"""
    Represent the end region (three last teeth) for the left side of the mouth.
    """*/
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_schema(46), 48, 3) ) {
        if(array_key_exists(48, $schema) AND to_be_replaced(get_condition(48, $schema), 48, $schema)
            AND array_key_exists(47, $schema) AND to_be_replaced(get_condition(47, $schema), 47, $schema)
            AND array_key_exists(46, $schema) AND to_be_replaced(get_condition(46, $schema), 46, $schema)
        ) {
            array_push($teeth_region_eveluate, 'mandible_left_end');

            return TRUE;
        }
    }
    else {
        return FALSE;
    }
}

function is_mandible_right_end($schema) {
    /*"""
    Represent the end region (three last teeth) for the right side of the mouth.
    """*/
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_schema(38), 36, 3) ) {
        if(array_key_exists(38, $schema) AND to_be_replaced(get_condition(38, $schema), 38, $schema)
            AND array_key_exists(37, $schema) AND to_be_replaced(get_condition(37, $schema), 37, $schema)
            AND array_key_exists(36, $schema) AND to_be_replaced(get_condition(36, $schema), 36, $schema)
        ) {
            array_push($teeth_region_eveluate, 'mandible_right_end');
            return TRUE;
        }
    }
    else {
        return FALSE;
    }
}

function is_mandible_front($teeth) {
    /*"""
    Represent the front (anterior) region (6 front teeth) of the mouth.
    """*/
    global $teeth_region_eveluate;

    if(sub_array_teeth(position_selected(33), 43, 6) ) {
        array_push($teeth_region_eveluate, 'mandible front');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function is_X7_X8($schema) {
    /*"""
    Represent the X7-X8 region of the mouth.
    """*/
    global $teeth_region_eveluate, $teeth_with_status;

    $to_be_replaced_count = [];

    foreach($schema as $key => $value) {
        if(to_be_replaced($value, $key, $schema)
        ) {
            array_push($to_be_replaced_count, $key);
        }
    }

    if(sub_array_teeth(position_selected(18), 17, 2) OR
        sub_array_teeth(position_selected(27), 28, 2) OR
        sub_array_teeth(position_selected(38), 37, 2) OR
        sub_array_teeth(position_selected(47), 48, 2)
    ) {
        if( (
            (array_key_exists(18, $schema) AND to_be_replaced(get_condition(18, $schema), 18, $schema)
            AND array_key_exists(17, $schema) AND to_be_replaced(get_condition(17, $schema), 17, $schema) )

            OR (array_key_exists(28, $schema) AND to_be_replaced(get_condition(28, $schema), 28, $schema)
            AND array_key_exists(27, $schema) AND to_be_replaced(get_condition(27, $schema), 27, $schema))

            OR (array_key_exists(38, $schema) AND to_be_replaced(get_condition(38, $schema), 38, $schema)
            AND array_key_exists(37, $schema) AND to_be_replaced(get_condition(37, $schema), 37, $schema))

            OR (array_key_exists(48, $schema) AND to_be_replaced(get_condition(48, $schema), 48, $schema)
            AND array_key_exists(47, $schema) AND to_be_replaced(get_condition(47, $schema), 47, $schema))
            )
            // AND count($to_be_replaced_count) == 2
        ) {
            array_push($teeth_region_eveluate, 'in_X7_X8');
            return TRUE;
        }
    }
    else {
        return FALSE;
    }
}

function is_interdental_gap($schema) {
    global $teeth_inter_dental_gaps;
    $teeth_inter_dental_gap_right = [];
    $teeth_inter_dental_gap_left = [];

    foreach ($schema as $tooth_number => $status) {
        // if(to_be_treated($status) OR $status == 'pw' OR $status == ')(') {
            
        //     if(right($tooth_number, $schema) 
        //         AND to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
        //         AND !in_array($tooth_number, [27, 37])
        //     ) {
        //         array_push($teeth_inter_dental_gap_right, $tooth_number);
        //     }

        //     if(left($tooth_number, $schema) 
        //         AND to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema)
        //         AND !in_array($tooth_number, [17, 47])
        //     ) {
        //         array_push($teeth_inter_dental_gap_left, $tooth_number);
        //     }
        // }

        if(to_be_replaced($status, $tooth_number, $schema)
            AND !in_array($tooth_number, [18, 28, 38, 48])
        ) {
            if(right($tooth_number, $schema) 
                AND (to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
                    OR to_be_treated(right($tooth_number, $schema)['status'])
                )
            ) {
                array_push($teeth_inter_dental_gap_right, $tooth_number);
            }

            if(left($tooth_number, $schema) 
                AND (to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema)
                    OR to_be_treated(left($tooth_number, $schema)['status'])
                )
            ) {
                array_push($teeth_inter_dental_gap_left, $tooth_number);
            }

        }
    }

    if(count($teeth_inter_dental_gap_right) == count($teeth_inter_dental_gap_left) 
    ) {
        $teeth_inter_dental_gaps = array_combine($teeth_inter_dental_gap_right, $teeth_inter_dental_gap_left);
    }

    $teeth_inter_dental_gaps = []; // ISSUES

    if(count($teeth_inter_dental_gaps) > 0)
        return TRUE;
    else
        return FALSE;
}

function biggest_interdental_gap($teeth_inter_dental_gaps) {
    $biggest_interdental_gap = [];
    foreach($teeth_inter_dental_gaps as $start => $end) {
        $biggest = abs(intval(position_schema($start)) - intval(position_schema($end))) - 1;
        array_push($biggest_interdental_gap, $biggest);
    }

    return max($biggest_interdental_gap);
}

function position_schema($tooth_number) {

    $position = array_search($tooth_number, $GLOBALS['TOOTH_NUMBERS_ISO'] );
    
    $position = ($position === FALSE) ? "" : $position;

    return $position; //$TOOTH_NUMBERS_ISO.index($tooth_number);
}

function position_selected($tooth_number) {
    $postition = array_search($tooth_number, $GLOBALS['teeth_with_status'] );
    if(! $postition)
        $postition = 0;
    return $postition;
}

function get_condition($tooth, $schema) {
    $condition = isset($schema[$tooth]) ? $schema[$tooth] : '';

    // return $schema[$tooth];
    return $condition;
}

function get_jaw($tooth) {
    global $TOOTH_NUMBERS_ISO;
    $TOOTH_NUMBERS_ISO_UPPER = array_slice($TOOTH_NUMBERS_ISO,0,16);
    $TOOTH_NUMBERS_ISO_LOWER = array_slice($TOOTH_NUMBERS_ISO,16,32);

    if(in_array($tooth, $TOOTH_NUMBERS_ISO_UPPER)) {
        return 'OK';
    }

    if(in_array($tooth, $TOOTH_NUMBERS_ISO_LOWER)) {
        return 'UK';
    }
}

function subsidy_exists($value) {
    global $teeth_subsidy_eveluate;

    for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
        if(startswith($teeth_subsidy_eveluate[$len]['subsidy'], $value)) {
            return TRUE;
        }
    }
}

function subsidy_exists_optional($value) {
    global $teeth_subsidy_eveluate_2x;

    for($len=0; $len<count($teeth_subsidy_eveluate_2x); $len++) {
        if(startswith($teeth_subsidy_eveluate_2x[$len]['subsidy'], $value)) {
            return TRUE;
        }
    }
}

function subsidy_exists_upper($value) {
    global $teeth_subsidy_eveluate_upper;

    for($len=0; $len<count($teeth_subsidy_eveluate_upper); $len++) {
        if(startswith($teeth_subsidy_eveluate_upper[$len]['subsidy'], $value)) {
            return TRUE;
        }
    }
}

function subsidy_exists_region($value) {
    global $teeth_subsidy_eveluate;

    $return_region = [];

    for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
        if(startswith($teeth_subsidy_eveluate[$len]['subsidy'], $value)) {
            array_push($return_region, $teeth_subsidy_eveluate[$len]['region']);
        }
    }

    return $return_region;
}

function subsidy_exists_region_2x($value) {
    global $teeth_subsidy_eveluate, $TOOTH_NUMBERS_ISO;

    $new_arr_rv = [];

    for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
        if(startswith($teeth_subsidy_eveluate[$len]['subsidy'], $value)) {

            $temp_2x = explode('-' , $teeth_subsidy_eveluate[$len]['region']);

            for($az=position_schema($temp_2x[0]); $az<=position_schema(end($temp_2x)); $az++ ) {
                array_push($new_arr_rv, $TOOTH_NUMBERS_ISO[$az]);
            }

            $new_arr_rv = array_unique($new_arr_rv);
        }
    }

    return $new_arr_rv;
}

function subsidy_exists_name($applied_rule) {
    global $teeth_subsidy_eveluate;

    for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
        if(startswith($teeth_subsidy_eveluate[$len]['applied_rule'], $applied_rule)) {
            return TRUE;
        }
    }
}

function subsidy_remove_name($applied_rule) {
    global $teeth_subsidy_eveluate;

    // remove dependencies or duplicates
    // print_r($teeth_subsidy_eveluate);
    $remove_subsidies = [];

    for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
        if(startswith($teeth_subsidy_eveluate[$len]['applied_rule'], $applied_rule)) {
            array_push($remove_subsidies, $len);
        }
    }

    for($rem=0; $rem<count($remove_subsidies); $rem++) {
        unset($teeth_subsidy_eveluate[$remove_subsidies[$rem]]);
    }

    $teeth_subsidy_eveluate = array_values(($teeth_subsidy_eveluate));
}

function atleastOneInPostRegion($schema) {
    /*"""
    Represent the Posterior region (X4-X8) of the mouth.
    """*/
    global $teeth_region_eveluate;

    $in_posterior = [];

    foreach($schema as $tooth_number => $status) {

        if( in_array($tooth_number, [18,17,16,15,14,24,25,26,27,28,38,37,36,35,34,44,45,46,47,48])
            AND (to_be_treated($status) OR $status == 'pw' 
                OR to_be_replaced(get_condition($tooth_number, $schema), $tooth_number, $schema)
                )
        ) {
            array_push($in_posterior, $tooth_number);
        }
    } 

    if(count($in_posterior) > 0 )
    {
        array_push($teeth_region_eveluate, 'atleastOneInPostRegion');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function inAnteriorRegion($schema) {
    /*"""
    Represent the anterior region (X3-X3) of the mouth.
    """*/
    global $teeth_region_eveluate, $teeth_with_status, $TOOTH_NUMBERS_ISO;
    $TOOTH_NUMBERS_ISO_UPPER = array_slice($TOOTH_NUMBERS_ISO,0,16);
    $TOOTH_NUMBERS_ISO_LOWER = array_slice($TOOTH_NUMBERS_ISO,16,32);

    $in_anterior = [];
    $upper_count = count(array_intersect($TOOTH_NUMBERS_ISO_UPPER, $teeth_with_status));
    $lower_count = count(array_intersect($TOOTH_NUMBERS_ISO_LOWER, $teeth_with_status));

    foreach($schema as $tooth_number => $status) {

        if( in_array($tooth_number, [13,12,11,21,22,23,33,32,31,41,42,43])
            AND (to_be_treated($status) OR $status == 'pw' 
                OR to_be_replaced(get_condition($tooth_number, $schema), $tooth_number, $schema)
                )
        ) {
            array_push($in_anterior, $tooth_number);
        }
    }

    if(count($in_anterior) == $upper_count OR count($in_anterior) == $lower_count )
    {
        array_push($teeth_region_eveluate, 'inAnteriorRegion');
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function is_neighbor_gap($tooth_number, $schema) {

    $left   = (($tooth_number !== 18 OR $tooth_number !== 38) 
                    AND !is_null(left(left($tooth_number, $schema)['tooth'], $schema))) ? 
                left(left($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];
    $right  = (($tooth_number !== 28 OR $tooth_number !== 48) 
                    AND !is_null(right(right($tooth_number, $schema)['tooth'], $schema)) ) ? 
                right(right($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];

    $neighbour_gap = '';

    // IGNORE the X8 from the schema
    $schema[18] = '';
    $schema[28] = '';
    $schema[38] = '';
    $schema[48] = '';

    if(in_array($tooth_number, [18,28,38,48])
    ) {
        $neighbour_gap = 0;
    }

    if($left['tooth'] == '18')
    {
        $left['status'] = '';
    }
    if($left['tooth'] == '38')
    {
        $left['status'] = '';
    }

    if($right['tooth'] == '28')
    {
        $right['status'] = '';
    }
    if($right['tooth'] == '48')
    {
        $right['status'] = '';
    }

    if( (to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
            OR to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema)
        )
        OR 
            (right($tooth_number, $schema)['status'] == ')('
            AND to_be_replaced($right['status'], $tooth_number, $schema))
        OR 
            (left($tooth_number, $schema)['status'] == ')('
            AND to_be_replaced($left['status'], $tooth_number, $schema))
    ) {
        $neighbour_gap = 0;
    }

    if(!to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
        AND !to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema) 
        AND ( to_be_replaced($right['status'], $tooth_number, $schema)
            OR to_be_replaced($left['status'], $tooth_number, $schema))
        AND $neighbour_gap !== 0
        // AND (!in_array($right['tooth'], [28,48])
        //     AND !in_array($left['tooth'], [18,38]))
    ) {
        $neighbour_gap = 1;
    }
    // if( $neighbour_gap == 1 AND
    //     (!to_be_replaced($right['status'], $tooth_number, $schema) OR //AND
    //     !to_be_replaced($left['status'], $tooth_number, $schema) )
    // ) {
    //     $neighbour_gap = 2;
    // }

    return $neighbour_gap;
}

function is_neighbor_gap_ob_f($tooth_number, $schema) {
    // $left   = ($tooth_number !== 18 OR $tooth_number !== 48) ? 
    //             left(left($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];
    // $right  = ($tooth_number !== 28 OR $tooth_number !== 38) ? 
    //             right(right($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];

    $left   = (($tooth_number !== 18 OR $tooth_number !== 38) 
                    AND !is_null(left(left($tooth_number, $schema)['tooth'], $schema))) ? 
                left(left($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];
    $right  = (($tooth_number !== 28 OR $tooth_number !== 48) 
                    AND !is_null(right(right($tooth_number, $schema)['tooth'], $schema)) ) ? 
                right(right($tooth_number, $schema)['tooth'], $schema) : ["status" => '', "tooth" => ''];

    $neighbour_gap = '';


    if( (to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
            OR to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema)
        )
        OR 
            (right($tooth_number, $schema)['status'] == ')('
            AND to_be_replaced($right['status'], $tooth_number, $schema))
        OR 
            (left($tooth_number, $schema)['status'] == ')('
            AND to_be_replaced($left['status'], $tooth_number, $schema))
    ) {
        $neighbour_gap = 0;
    }

    if(!to_be_replaced(right($tooth_number, $schema)['status'], $tooth_number, $schema)
        AND !to_be_replaced(left($tooth_number, $schema)['status'], $tooth_number, $schema) 
        AND (to_be_replaced($right['status'], $tooth_number, $schema) 
            OR to_be_replaced($left['status'], $tooth_number, $schema))
        AND $neighbour_gap !== 0
    ) {
        $neighbour_gap = 1;
    }

    return $neighbour_gap;
}

function veneering_grants($tooth_number) {
    $veneering_grants = false;

    if( in_array($tooth_number, [15,14,13,12,11,21,22,23,24,25,34,33,32,31,41,42,43,44]) ) {
        $veneering_grants = true;
    }

    return $veneering_grants;

}

function right($tooth_number, $schema) {
    /*"""
    Return the tooth that is just in the right of this object
    """*/

    $status = ''; 
    $tooth  = '';

    // if($position_schema == 31) {
    if($tooth_number == "28" OR $tooth_number == "48" 
        OR $tooth_number == ""
    ) {
        // return ["status" => '', "tooth" => ''];
    }
    else {
        $position_schema = position_schema($tooth_number);
        
        $position_schema = intval($position_schema) + 1;

        $tooth = !is_null($GLOBALS['TOOTH_NUMBERS_ISO'][$position_schema]) ?
                    $GLOBALS['TOOTH_NUMBERS_ISO'][$position_schema] : '';

        $status = get_condition($tooth, $schema);

        return ["status" => $status, "tooth" => $tooth];
    }
}

function left($tooth_number, $schema) {
    /*"""
    Return the tooth that is just in the left of this object
    """*/

    $status = ''; 
    $tooth  = '';

    if($tooth_number == "18" OR $tooth_number == "38" OR $tooth_number == "") {
        // return ["status" => '', "tooth" => ''];
    }
    else {
        $position_schema = position_schema($tooth_number);

        $position_schema = intval($position_schema) - 1;

        $tooth = !is_null($GLOBALS['TOOTH_NUMBERS_ISO'][$position_schema]) ?
                    $GLOBALS['TOOTH_NUMBERS_ISO'][$position_schema] : '';

        // $status = $schema[$tooth];
        $status = get_condition($tooth, $schema);

        return ["status" => $status, "tooth" => $tooth];
    }
}

function to_be_replaced_count($start, $end, $schema) {
    /*"""
    Count how many TBR teeth are in some specific region.
    """*/
    $to_be_replaced_count = [];

    // if ($region == NULL)
    // {
    //     $region = new Region();
    //     $region = $region->whole_mouth();
    // } 0 , 2

    if($start < $end)
    {
        foreach($schema as $key => $value) {
            if(to_be_replaced($value) AND position_schema($key)<= $end) {
                array_push($to_be_replaced_count, $value);
            }
        }
    }

    return count($to_be_replaced_count);
}

function to_be_replaced($condition, $tooth_number, $schema) {
    /*"""
    To Be Replaced. This can be:
        a) f
        b) x
        c) b + neighboring finding (ww/kw/tw/pw/rw/x)
        d) ew
        e) sw
        f) bw
    """*/
    if(!$condition) {
        $condition = '';
    }

    //if ( in_array($condition, ["f", "x", "ew", "sw", "fi", "bw"])) {
    // if ( in_array($condition, ["f", "x", "ew", "sw", "bw", "kx"])) {
    if ( in_array($condition, ["f", "x", "ew", "bw", "kx","swfb"])) {
        return True;
    }

    /*if ($condition == "b") {
        // If the teeth near to this one is a TBR
        if (to_be_treated(left($tooth_number, $schema)['status']) OR
            to_be_treated(right($tooth_number, $schema)['status'])
        ) {
            return True;
        }

        // // If the teeth near to this one is an "x"
        if (left($tooth_number, $schema)['status'] == "x" OR
            right($tooth_number, $schema)['status'] == "x") 
        {
            return True;
        }

        // If the teeth near to this one is also a "b" (bridge)
        // and the last "b" is at the side of a TBT
        // Check from the left side
        // $left = left($tooth_number, $schema);
        // $left = left(left($tooth_number, $schema)['tooth'], $schema);

        // while ($left) {
        //     if (left($tooth_number, $schema)['status'] == "b") {
        //         $left = left(left($tooth_number, $schema)['tooth'], $schema);
        //     }
        //     else if (to_be_treated(left($tooth_number, $schema)['status'])) {
        //         return True;
        //     }
        //     else {
        //         break;
        //     }
        // }

        // // Check from the right side
        // $right = $this->right;

        // while ($right) {
        //     if ($right->condition == "b")
        //         $right = $right->right;
        //     else if ($right->to_be_treated)
        //         return True;
        //     else {
        //         break;
        //     }
        // }
    }*/

    return False;
}

function to_be_treated($condition) {
    /*"""
    To Be Treated are existing tooth with or without findings:
        ww/kw/tw/pw/rw.
    """*/
    if (in_array($condition, ["ww", "kw", "tw", "pw", "rw", "ur"])) {
    // if (in_array($condition, ["ww", "kw", "tw", "rw", "ur", "k"])) {
        return True;
    }
    return False;
}

function to_be_sw($condition) {
    /*"""
    To Be 7.x are existing tooth with or without findings:
        sw
    """*/
    if (in_array($condition, ["sw"])) {
        return True;
    }
    return False;
}

function to_be_hs($condition) {
    /** RV shortcuts for 3.1 as H */
    if(in_array($condition, ["", "k", "pk", "a", "K", "KV", "KM", "PK", "PKV", "PKM", "A"])
    ) {
        return TRUE;
    }
    return FALSE;
}

function to_be_treated_replaced($condition) {
    /*"""
    To Be Treated are existing tooth with or without findings:
        ww/kw/tw/pw/rw.
    """*/
    if (in_array($condition, ["ww", "kw", "tw", "pw", "rw", "ur", "f", "x", "ew", "sw", "bw", "b", ")("])) {
    // if (in_array($condition, ["ww", "kw", "tw", "rw", "ur", "k"])) {
        return True;
    }
    return False;
}

function Exact16ToBeReplacedTeeth_upper($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];

    // if (in_array('upper_jaw', $teeth_region_eveluate) ) {
        $i =0;
        foreach($schema as $key => $value) {
            if( (to_be_replaced($value, $key, $schema) 
                OR $value == 'b' )
                AND position_schema($key)<16
            ) {
                array_push($to_be_replaced, $value);
            }
            $i++;
        }
    // }

    if (count($to_be_replaced) == 16) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.2", "output_order"=> "1", "region"=> get_jaw(16), "quantity"=> "1", "applied_rule"=> "Exact16ToBeReplacedTeeth_upper"]
        );

        // return True;
    }
}

function Exact16ToBeReplacedTeeth_mandible($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];

    // if (in_array('mandible', $teeth_region_eveluate) ) {
        $i = 16;
        foreach($schema as $key => $value) {
            if( (to_be_replaced($value, $key, $schema) 
                OR $value == 'b' )
                AND position_schema($key) > 15
                AND position_schema($key) < 32
            ) {
                array_push($to_be_replaced, $value);
            }
            $i++;
        }
    // }

    if (count($to_be_replaced) == 16) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.4", "output_order"=> "2", "region"=> get_jaw(36), "quantity"=> "1", "applied_rule"=> "Exact16ToBeReplacedTeeth_mandible"]
        );

        // return True;
    }
}

function Between13And15ToBeReplacedTeeth_upper($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];
    $upper_all = [18, 17, 16, 15, 14, 13, 12, 11, 21, 22, 23, 24, 25, 26, 27, 28];

    if(subsidy_exists(4.2) OR subsidy_exists_upper(4.2)
    ) {
        return FALSE;
    }

    // if (in_array('upper_jaw', $teeth_region_eveluate) ) {
        foreach($schema as $key => $value) {
            if((to_be_replaced($value, $key, $schema) 
                OR $value  == ')(')
                AND position_schema($key)<16) {
                array_push($to_be_replaced, $key);
            }

            if(($value == 'b' 
                AND
                    (to_be_replaced(left($key, $schema)['status'], $key, $schema)
                    OR to_be_replaced(right($key, $schema)['status'], $key, $schema)
                    )
                )
                AND position_schema($key)<16) {
                array_push($to_be_replaced, $key);
            }
        }
    // }

    $remaining_assoc = array_diff($upper_all, $to_be_replaced);
    $remaining = [];
    foreach($remaining_assoc as $pos => $val)
    {
        if(to_be_treated(get_condition($val, $schema))
            OR get_condition($val, $schema) == 'k'
            OR get_condition($val, $schema) == ''
        ) {
            array_push($remaining, $val);
        }
    }
    
    if (13 <= count($to_be_replaced) and count($to_be_replaced) <= 15) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.1", "output_order"=> "3", "region"=> get_jaw(16), "quantity"=> "1", "applied_rule"=> "Between13And15ToBeReplacedTeeth_upper"]
        );

        for($x=0; $x<count($remaining); $x++) {
            array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.6", "output_order"=> "5", "region"=> $remaining[$x], "quantity"=> "1", "applied_rule"=> "Between13And15ToBeReplacedTeeth_upper"]
            );

            if( veneering_grants($remaining[$x]) ){
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $remaining[$x], "quantity"=> "1", "applied_rule"=> "veneering_grants 1"]
                );
            }
        }

        return True;
    }
}

function Between13And15ToBeReplacedTeeth_mandible($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];
    $lower_all = [38, 37, 36, 35, 34, 33, 32, 31, 41, 42, 43, 44, 45, 46, 47, 48];

    if(subsidy_exists(4.4) OR subsidy_exists_upper(4.4)
    ) {
        return FALSE;
    }

    // if (in_array('mandible', $teeth_region_eveluate) ) {
        foreach($schema as $key => $value) {
            if((to_be_replaced($value, $key, $schema) 
                OR $value  == ')(')
                AND position_schema($key)>15) {
                array_push($to_be_replaced, $key);
            }

            if(($value == 'b' 
                AND
                    (to_be_replaced(left($key, $schema)['status'], $key, $schema)
                    OR to_be_replaced(right($key, $schema)['status'], $key, $schema)
                    )
                )
                AND position_schema($key)>15) {
                array_push($to_be_replaced, $key);
            }
        }
    // }

    $remaining_assoc = array_diff($lower_all, $to_be_replaced);
    $remaining = [];
    foreach($remaining_assoc as $pos => $val)
    {
        if(to_be_treated(get_condition($val, $schema))
            OR get_condition($val, $schema) == 'k'
            OR get_condition($val, $schema) == ''
        ) {
            array_push($remaining, $val);
        }
    }
    
    if (13 <= count($to_be_replaced) and count($to_be_replaced) <= 15) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.3", "output_order"=> "4", "region"=> get_jaw(36), "quantity"=> "1", "applied_rule"=> "Between13And15ToBeReplacedTeeth_mandible"]
        );

        for($x=0; $x<count($remaining); $x++) {
            array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "4.6", "output_order"=> "5", "region"=> $remaining[$x], "quantity"=> "1", "applied_rule"=> "Between13And15ToBeReplacedTeeth_upper"]
            );

            if( veneering_grants($remaining[$x]) ){
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $remaining[$x], "quantity"=> "1", "applied_rule"=> "veneering_grants 2"]
                );
            }
        }

        return True;
    }
}

/** 3.x starts **/
function Between5And12ToBeReplacedTeeth_upper($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];

    if(subsidy_exists(4.1) OR subsidy_exists(4.2)
        OR subsidy_exists_upper(4.1) OR subsidy_exists_upper(4.2)
        OR subsidy_exists_upper(3.1)
    ) {
        return FALSE;
    }

    // $upper_jaw_without_x8 = new Region(17, 27);

    // if (in_array('upper_jaw', $teeth_region_eveluate) ) {
        foreach($schema as $key => $value) {
            if(to_be_replaced($value, $key, $schema) AND position_schema($key)>0 AND position_schema($key)<15) {
                array_push($to_be_replaced, $key);
            }
        }
    // }
    
    if (5 <= count($to_be_replaced) and count($to_be_replaced) <= 12) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw(16), "quantity"=> "1", "applied_rule"=> "Between5And12ToBeReplacedTeeth_upper"]
        );

        return TRUE;
    }
}

function Between5And12ToBeReplacedTeeth_mandible($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];

    if(subsidy_exists(4.3) OR subsidy_exists(4.4)
        OR subsidy_exists_upper(4.3) OR subsidy_exists_upper(4.4)
    ) {
        return FALSE;
    }

    // $mandible_without_x8 = new Region(37, 47);

    // if (in_array('upper_jaw', $teeth_region_eveluate) ) {
        foreach($schema as $key => $value) {
            
            if(to_be_replaced($value, $key, $schema) AND position_schema($key)>16 AND position_schema($key)<31) {
                array_push($to_be_replaced, $value);
            }
        }
    // }
    
    if (5 <= count($to_be_replaced) and count($to_be_replaced) <= 12) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw(36), "quantity"=> "1", "applied_rule"=> "Between5And12ToBeReplacedTeeth_upper"]
        );

        return True;
    }
}

function X7_X8_check($schema)
{
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;
    $to_be_replaced = [];

    if(subsidy_exists(4)
        // OR subsidy_exists_upper(4)
    ) {
        return FALSE;
    }

    foreach($schema as $key => $value) {
        if(to_be_replaced($value, $key, $schema)
        ) {
            array_push($to_be_replaced, $key);
        }
    }

    if(//count($to_be_replaced_count) == 2 AND // ISSUES
        (
        (in_array(18, $to_be_replaced) AND in_array(17, $to_be_replaced) 
            //AND (in_array(16, $to_be_replaced) OR get_condition(16, $schema) == '')
        )
        OR
        (in_array(27, $to_be_replaced) AND in_array(28, $to_be_replaced)
            //AND (in_array(26, $to_be_replaced) OR get_condition(26, $schema) == '')
        )
        )
    ) {
        subsidy_remove_name('Between5And12ToBeReplacedTeeth'); //ISSUES

        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "OK", "quantity"=> "1", "applied_rule"=> "X7_X8_Upper_"]
        );
    }

    if((
        (in_array(37, $to_be_replaced) AND in_array(38, $to_be_replaced)
            //AND (in_array(36, $to_be_replaced) OR get_condition(36, $schema) == '')
        )
        OR
        (in_array(47, $to_be_replaced) AND in_array(48, $to_be_replaced)
            //AND (in_array(46, $to_be_replaced) OR get_condition(46, $schema) == '')
        )
        )
    ) {
        subsidy_remove_name('Between5And12ToBeReplacedTeeth'); //ISSUES

        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "UK", "quantity"=> "1", "applied_rule"=> "X7_X8_Lower_"]
        );
    }
}

function X7_X8_3x_2x($schema)
{
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status, $is_optional_3x_2x,
            $teeth_subsidy_eveluate_2x, $teeth_subsidy_eveluate_upper, $is_optional_3x_1x;
            
    $to_be_replaced = [];

    if(subsidy_exists(4)
        // OR subsidy_exists_upper(4)
    ) {
        return FALSE;
    }

    foreach($schema as $key => $value) {
        if(to_be_replaced($value, $key, $schema)
        ) {
            array_push($to_be_replaced, $key);
        }
    }
    
    if(
        (
        (in_array(18, $to_be_replaced) AND in_array(17, $to_be_replaced)
        )
        OR
        (in_array(27, $to_be_replaced) AND in_array(28, $to_be_replaced)
        )
        )
        // AND !in_array(16, $to_be_replaced)
        // AND !in_array(26, $to_be_replaced)
        AND get_condition(16, $schema) == ''
        AND get_condition(26, $schema) == ''
    ) {
        $is_optional_3x_2x = TRUE;
        $teeth_subsidy_eveluate_2x = array_merge($teeth_subsidy_eveluate_2x, $teeth_subsidy_eveluate);

        $teeth_subsidy_eveluate = [];
        $is_optional_3x_1x = TRUE;
    }

    if((
        (in_array(37, $to_be_replaced) AND in_array(38, $to_be_replaced)
        )
        OR
        (in_array(47, $to_be_replaced) AND in_array(48, $to_be_replaced)
        )
        )
        // AND !in_array(36, $to_be_replaced)
        // AND !in_array(46, $to_be_replaced)
        AND get_condition(36, $schema) == ''
        AND get_condition(46, $schema) == ''
    ) {
        $is_optional_3x_2x = TRUE;

        $teeth_subsidy_eveluate_2x = array_merge($teeth_subsidy_eveluate_2x, $teeth_subsidy_eveluate);
        $teeth_subsidy_eveluate = [];
        $is_optional_3x_1x = TRUE;
    }
}

function UnilateralFreeEndToBeReplacedTeethAtLeast1_upper($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;

    // if(subsidy_exists("3.1"))
    // {
    //     // subsidy_remove_name('Between5And12ToBeReplacedTeeth'); //ISSUES
    //     // return FALSE;
    // }
    if(subsidy_exists(4.1) OR subsidy_exists(4.2) 
        OR subsidy_exists_upper(4.1) OR subsidy_exists_upper(4.2)
        OR subsidy_exists_upper(3.1)
    ) {
        return FALSE;
    }

    if(subsidy_exists_name('UnilateralFreeEndToBeReplacedTeethAtLeast1_upper'))
    {
        return FALSE;
    }

    // MAY NOT REQUIRED AS SAME AS X7_X8
    // if ((in_array('upper_jaw_left_end', $teeth_region_eveluate) 
    //     OR in_array('upper_jaw_right_end', $teeth_region_eveluate))
    //     // AND count($teeth_with_status) < 5
    // ) {
    //     subsidy_remove_name('Between5And12ToBeReplacedTeeth'); //ISSUES
    //     subsidy_remove_name('X7_X8'); //ISSUES

    //     array_push($teeth_subsidy_eveluate, 
    //             ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw(16), "quantity"=> "1", "applied_rule"=> "UnilateralFreeEndToBeReplacedTeethAtLeast1_upper"]
    //     );
    // }
}

function UnilateralFreeEndToBeReplacedTeethAtLeast1_mandible($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;

    if(subsidy_exists(4.3) OR subsidy_exists(4.4)
        OR subsidy_exists_upper(4.3) OR subsidy_exists_upper(4.4)
    ) {
        return FALSE;
    }

    if(subsidy_exists_name('UnilateralFreeEndToBeReplacedTeethAtLeast1_mandible'))
    {
        return FALSE;
    }

    // MAY NOT REQUIRED AS SAME AS X7_X8
    // if ((in_array('mandible_left_end', $teeth_region_eveluate) 
    //     OR in_array('mandible_right_end', $teeth_region_eveluate))
    //     // AND count($teeth_with_status) < 5
    // ) {
    //     subsidy_remove_name('Between5And12ToBeReplacedTeeth'); //ISSUES
    //     subsidy_remove_name('X7_X8'); //ISSUES

    //     array_push($teeth_subsidy_eveluate, 
    //             ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw(36), "quantity"=> "1", "applied_rule"=> "UnilateralFreeEndToBeReplacedTeethAtLeast1_mandible"]
    //     );
    // }
}

function ExactToBeReplacedTeethInterdentalGapInFronRegion_upper($schema) {
    /**"""
    if upper jaw && interdental gap TBR=1// all in region 12-22.

    It is only fired when no other 3.X rule was fired before it.

    Notice that the 2.1 / 2.2 is only possible if 13 != PT -
    in other words: if we have a 3.2 situation (and this is needed for
    2.1+2.2 to happen) with the 13 or 23 being the first good tooth
    (would be a potential telescope = PT) it will NOT be a 2.1 case,
    as the 13/23 can only be a T (as part of the free end)
    or an AT for the gap.

    In Issue #133 (21/05/2019) we also added a new rule: X2 should be
    existing on both sides to this rule to be fired.
    """**/

    $to_be_replaced_count = [];
    $subsidy = NULL;

    // 2.1/2.2: if upper jaw && X6-X8 on one side and at
    // least X7-X8 as minimum covered on the other side

    // At least one side should have TBR=3
    if (! to_be_replaced_count(position_selected(18), position_selected(16), $schema) == 3 AND
        ! to_be_replaced_count(position_selected(26), position_selected(28), $schema) == 3
    ) {
        return False;
    }

    // If 18-16 is the one with TBR=3, the other side should have
    // minimum X7-X8
    if (to_be_replaced_count(position_selected(18), position_selected(16), $schema) == 3 AND
        ! to_be_replaced_count(position_selected(27), position_selected(28), $schema) == 2
    ) {
        return False;
    }

    // If 26-28 is the one with TBR=3, the other side should have
    // minimum X7-X8
    if (
        to_be_replaced_count(position_selected(26), position_selected(28), $schema) == 3 AND
        ! to_be_replaced_count(position_selected(18), position_selected(17), $schema) == 2
    ) {
        return False;
    }

    // As defined in Issue #133, missing tooth must be inside 12-22
    // area, that's the reason by us starting from 13-23 here.
    $interdental_gap = $this->schema.interdental_gap(
        $region=new Region(13, 23, $this->schema)
    );

    if (! $interdental_gap) {
        return False;
    }

    // Issue #133: we have found on this issue that we would not consider a
    // this rule as valid if we have two gaps in front region at the same
    // time. The only possibility for this case is a gap in 13-11 and another
    // in 11-22
    if (
        count(
            $this->schema.interdental_gaps($region=new Region(13, 23, $this->schema))
        )!= 1) {
        return False;
    }

    // X2 should be
    // existing on both sides to this rule to be fired. X2 can only be
    // missing when it is part of the interdental gap found by the rule.
    $tooth_12 = new Region(12, 12, $this->schema);
    $tooth_22 = new Region(22, 22, $this->schema);

    if (($tooth_12->to_be_replaced and ! in_array($tooth_12, $interdental_gap)) or (
        $tooth_22->to_be_replaced and ! in_array($tooth_22, $interdental_gap))
    ) {
        return False;
    }

    // As defined in Issue #133, a tooth in interdental gap can not be a
    // Telescope if it is the first TBT on a 3.1 case
    $teeth_32 = $this->identified_subsidies.teeth($subsidy_code="3.2");

    if (
        in_array($interdental_gap.a_tooth, $teeth_32) 
        or in_array($interdental_gap.b_tooth, $teeth_32)
    ) {
        return False;
    }

    if ($interdental_gap->to_be_replaced_count == $this->to_be_replaced_count) {
        array_push($this->identified_subsidies->list,
            [
                "subsidy"=> $this->subsidy,
                "region"=> $interdental_gap.teeth_marking_abutment_tooth,
                "applied_rule"=> self,
            ]
        );
        return True;
    }

    
}

function ExactToBeReplacedTeethInterdentalGapInFronRegion_mandible() {
    return False;
}

function Exact2ToBeReplacedTeethInterdentalGapInFrontRegion() {
    ExactToBeReplacedTeethInterdentalGapInFronRegion();
    $to_be_replaced_count = 2;
    $subsidy = "2.2";
    return ["to_be_replaced_count" => $to_be_replaced_count, "subsidy" => $subsidy];
}

function BilateralFreeEnd_upper($schema, $teeth_with_status) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status, 
        $teeth_inter_dental_gaps, $TOOTH_NUMBERS_ISO, $is_gap_closure_arr;

    if(subsidy_exists(4) //OR subsidy_exists_upper(4)
    ) {
        return FALSE;
    }

    $uni_bi_array = [];

    $q1 = 0;
    $q2 = 0;

    $q1_arr = [];
    $q2_arr = [];
    $q3_arr = [];
    $q4_arr = [];

    $q1_arr_2x = [];
    $q2_arr_2x = [];
    $q1_arr_2x_mid = [];
    $q2_arr_2x_mid = [];

    $q4_op_right    = 0;
    $q3_op_left     = 0;
    $q2_op_right    = 0;
    $q1_op_left     = 0;
    $interdental    = 0;

    $q4_op_right_arr    = [];
    $q3_op_left_arr     = [];
    $q2_op_right_arr    = [];
    $q1_op_left_arr     = [];
    $q4_op_right_arr_2  = [];
    $q3_op_left_arr_2   = [];
    $q2_op_right_arr_2  = [];
    $q1_op_left_arr_2   = [];

    $interdental_arr    = [];

    $a1_arr     = [];
    $b1_arr     = [];
    $a2_arr     = [];
    $b2_arr     = [];

    $a1_arr_x   = [];
    $b1_arr_x   = [];
    $a2_arr_x   = [];
    $b2_arr_x   = [];

    $a1_up_arr  = [];
    $b1_up_arr  = [];
    $a2_up_arr  = [];
    $b2_up_arr  = [];

    $uni1_arr  = [];
    $uni2_arr  = [];
    $uni3_arr  = [];
    $uni4_arr  = [];

    $a1 = 0;
    $b1 = 0;
    $a2 = 0;
    $b2 = 0;

    $a1_up = 0;
    $b1_up = 0;
    $a2_up = 0;
    $b2_up = 0;

    foreach($schema as $teeth => $status) {
        if(in_array($teeth, [18,17,16])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q1 ++;
                array_push($q1_arr, $teeth);
            }
        }

        if(in_array($teeth, [26,27,28])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q2++;
                array_push($q2_arr, $teeth);
            }
        }

        if(in_array($teeth, [38,37,36])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q3_arr, $teeth);
            }
        }

        if(in_array($teeth, [46,47,48])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q4_arr, $teeth);
            }
        }

        if(in_array($teeth, [15, 14])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q1_op_left ++;
                array_push($q1_op_left_arr, $teeth);
            }
        }
        if(in_array($teeth, [15, 16])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q1_op_left_arr_2, $teeth);
            }
        }

        if(in_array($teeth, [24, 25])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q2_op_right ++;
                array_push($q2_op_right_arr, $teeth);
            }
        }
        if(in_array($teeth, [26, 25])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q2_op_right_arr_2, $teeth);
            }
        }

        if(in_array($teeth, [35, 34])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q3_op_left ++;
                array_push($q3_op_left_arr, $teeth);
            }
        }
        if(in_array($teeth, [35, 36])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q3_op_left_arr_2, $teeth);
            }
        }

        if(in_array($teeth, [44, 45])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                $q4_op_right ++;
                array_push($q4_op_right_arr, $teeth);
            }
        }
        if(in_array($teeth, [46, 45])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($q4_op_right_arr_2, $teeth);
            }
        }

        if(in_array($teeth, [18,17,16,15,14])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($uni1_arr, $teeth);
            }
        }
        if(in_array($teeth, [28,27,26,25,24])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($uni2_arr, $teeth);
            }
        }
        if(in_array($teeth, [38,37,36,35,34])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($uni3_arr, $teeth);
            }
        }
        if(in_array($teeth, [48,47,46,45,44])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($uni4_arr, $teeth);
            }
        }

        // BILATERAL SECOND START
        if(in_array($teeth, [17, 16])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($a1_arr, $teeth);
            }
        }
        if(in_array($teeth, [15, 14])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($a1_arr_x, $teeth);
            }
        }

        if(in_array($teeth, [27, 26])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($a2_arr, $teeth);
            }
        }
        if(in_array($teeth, [25, 24])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($a2_arr_x, $teeth);
            }
        }

        if(in_array($teeth, [36, 37])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($b1_arr, $teeth);
            }
        }
        if(in_array($teeth, [34, 35])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($b1_arr_x, $teeth);
            }
        }

        if(in_array($teeth, [46, 47])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($b2_arr, $teeth);
            }
        }
        if(in_array($teeth, [44, 45])) {
            if(to_be_replaced($status, $teeth, $schema) OR $status == 'b' ) {
                array_push($b2_arr_x, $teeth);
            }
        }
        // BILATERAL SECOND END

        // 2.x ers
        if(in_array($teeth, [18,17])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($q1_arr_2x, $teeth);
            }
        }
        if(in_array($teeth, [16,15])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($q1_arr_2x_mid, $teeth);
            }
        }

        if(in_array($teeth, [28,27])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($q2_arr_2x, $teeth);
            }
        }
        if(in_array($teeth, [26,25])) {
            if(to_be_replaced($status, $teeth, $schema) ) {
                array_push($q2_arr_2x_mid, $teeth);
            }
        }

        if(in_array($teeth, [12,11,21,22])
            // AND (!to_be_replaced(get_condition(13, $schema), 13, $schema) 
            //         // AND get_condition(13, $schema) !== 'b'
            //     ) 
            // AND (!to_be_replaced(get_condition(23, $schema), 23, $schema)
            //         // AND get_condition(23, $schema) !== 'b'
            //     )
        ) {
            if(to_be_replaced($status, $teeth, $schema)
            ) {
                $interdental ++;
                array_push($interdental_arr, $teeth);
            }
        }
    }


    if($q1 >= 3 AND $q2 >= 3) {
        // subsidy_remove_name('Between5And12ToBeReplacedTeeth');
        // subsidy_remove_name('UnilateralFreeEndToBeReplacedTeethAtLeast1_upper');
        // subsidy_remove_name('X7_X8_Upper');

        // array_push($teeth_subsidy_eveluate, 
        //         ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "OK", "quantity"=> "1", "applied_rule"=> "BilateralFreeEnd_upper 1"]
        // );

        $uni_bi_array = [];
        array_push($uni_bi_array, 'bilat_upper');
    }

    if(count($q3_arr) >= 3 AND count($q4_arr) >= 3) {
        // subsidy_remove_name('Between5And12ToBeReplacedTeeth');
        // subsidy_remove_name('UnilateralFreeEndToBeReplacedTeethAtLeast1_mandible');
        // subsidy_remove_name('X7_X8_Lower');

        // array_push($teeth_subsidy_eveluate, 
        //         ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "UK", "quantity"=> "1", "applied_rule"=> "BilateralFreeEnd_mandible"]
        // );

        $uni_bi_array = [];
        array_push($uni_bi_array, 'bilat_lower');
    }

    if(($q1 >= 3 OR $q2 >= 3)
        AND !in_array('bilat_upper', $uni_bi_array)
    ) {
        $uni_bi_array = [];
        array_push($uni_bi_array, 'unilat_upper');
    }

    if((count($q3_arr) == 3 OR count($q4_arr) == 3)
        AND !in_array('bilat_lower', $uni_bi_array)
    ) {
        $uni_bi_array = [];
        array_push($uni_bi_array, 'unilat_lower');
    }
    
    if(//$q1 == 3 AND $q2 == 3 
        subsidy_exists(3.1) AND
        // in_array('bilat_upper', $uni_bi_array)
        (count($q1_arr) >= 3 AND count($q2_arr) >= 3)
        AND !subsidy_exists_name('Between5And12ToBeReplacedTeeth')
        AND $q1_op_left>=1 AND $q2_op_right >= 1
        AND in_array(15, $q1_op_left_arr) AND in_array(25, $q2_op_right_arr)
        // AND $interdental == 0
    ) {
        $first_val_3_2  = get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema) == 'b' 
                            ? $TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +2] : $TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1];
        
        $second_val_3_2 = get_condition($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema) == 'b'
                            ? $TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-2] : $TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1];


        if((    !to_be_replaced(get_condition($first_val_3_2, $schema),
                    $first_val_3_2, $schema)
            AND 
            (get_condition($first_val_3_2, $schema) !== ')(' 
                AND get_condition($first_val_3_2, $schema) !== 'i' )
        )
        AND
        (   !to_be_replaced(get_condition($second_val_3_2, $schema),
                    $second_val_3_2, $schema)
            AND
            (get_condition($second_val_3_2, $schema) !== ')('
                AND get_condition($second_val_3_2, $schema) !== 'i')
        )
        // if(to_be_treated(get_condition($first_val_3_2, $schema))
        //     AND
        //     to_be_treated(get_condition($second_val_3_2, $schema))
        ) {
            array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $first_val_3_2.','.$second_val_3_2, "quantity"=> "2", "applied_rule"=> "BilateralFreeEnd_upper"]
            );
            if( veneering_grants($first_val_3_2) 
                AND !in_array($first_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $first_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 3"]
                );
            }
            if( veneering_grants($second_val_3_2) 
                AND !in_array($second_val_3_2, $is_gap_closure_arr)
            ){
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $second_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 4"]
                );
            }
        }
    }

    if(subsidy_exists(3.1) AND
        // in_array('bilat_lower', $uni_bi_array)
        (count($q3_arr) >= 3 AND count($q4_arr) >= 3)
        AND !subsidy_exists_name('Between5And12ToBeReplacedTeeth')
        AND $q3_op_left>=1 AND $q4_op_right >= 1
        AND in_array(35, $q3_op_left_arr) AND in_array(45, $q4_op_right_arr)
        // AND $interdental == 0
    ) {
        $first_val_3_2  = get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], $schema) == 'b' 
                            ? $TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +2] : $TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1];
        
        $second_val_3_2 = get_condition($TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], $schema) == 'b'
                            ? $TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-2] : $TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1];

        if((    !to_be_replaced(get_condition($first_val_3_2, $schema),
                        $first_val_3_2, $schema)
                AND 
                (get_condition($first_val_3_2, $schema) !== ')('
                AND get_condition($first_val_3_2, $schema) !== 'i' )
            )
            AND
            (   !to_be_replaced(get_condition($second_val_3_2, $schema),
                        $second_val_3_2, $schema)
                AND
                get_condition($second_val_3_2, $schema) !== ')('
            )
        // if(to_be_treated(get_condition($first_val_3_2, $schema))
        //     AND
        //     to_be_treated(get_condition($second_val_3_2, $schema))
        ) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $first_val_3_2.','.$second_val_3_2, "quantity"=> "2", "applied_rule"=> "BilateralFreeEnd_upper"]
            );
            if( veneering_grants($first_val_3_2) 
                AND !in_array($first_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $first_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 3"]
                );
            }
            if( veneering_grants($second_val_3_2) 
                AND !in_array($second_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $second_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 4"]
                );
            }
        }
    }

    // BILIt Second Start
    if( (count($a1_arr) >= 2 AND count($a2_arr) >= 2)
        AND count($a1_arr_x) >= 1 AND count($a2_arr_x) >= 1
        AND in_array(15, $a1_arr_x) AND in_array(25, $a2_arr_x)
        // AND $interdental == 0
    ) {
        subsidy_remove_name('Between5And12ToBeReplacedTeeth');
        subsidy_remove_name('UnilateralFreeEndToBeReplacedTeethAtLeast1_upper');

        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "OK", "quantity"=> "1", "applied_rule"=> "BilateralFreeEnd_upper"]
        );

        $first_val_3_2  = get_condition($TOOTH_NUMBERS_ISO[position_schema(end($a1_arr_x)) +1], $schema) == 'b' 
                            ? $TOOTH_NUMBERS_ISO[position_schema(end($a1_arr_x)) +2] : $TOOTH_NUMBERS_ISO[position_schema(end($a1_arr_x)) +1];
        
        $second_val_3_2 = get_condition($TOOTH_NUMBERS_ISO[position_schema($a2_arr_x[0])-1], $schema) == 'b'
                            ? $TOOTH_NUMBERS_ISO[position_schema($a2_arr_x[0])-2] : $TOOTH_NUMBERS_ISO[position_schema($a2_arr_x[0])-1];


        if((    !to_be_replaced(get_condition($first_val_3_2, $schema),
                    $first_val_3_2, $schema)
            AND 
            (get_condition($first_val_3_2, $schema) !== ')('
                AND get_condition($first_val_3_2, $schema) !== 'i' )
        )
        AND
        (   !to_be_replaced(get_condition($second_val_3_2, $schema),
                    $second_val_3_2, $schema)
            AND
            get_condition($second_val_3_2, $schema) !== ')('
        )
        // if(to_be_treated(get_condition($first_val_3_2, $schema))
        //     AND
        //     to_be_treated(get_condition($second_val_3_2, $schema))
        ) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $first_val_3_2.','.$second_val_3_2, "quantity"=> "2", "applied_rule"=> "BilateralFreeEnd_upper"]
            );
            if( veneering_grants($first_val_3_2) 
                AND !in_array($first_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $first_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 3"]
                );
            }
            if( veneering_grants($second_val_3_2) 
                AND !in_array($second_val_3_2, $is_gap_closure_arr)
            ){
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $second_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 4"]
                );
            }
        }
    }

    if(count($b1_arr) >= 2 AND count($b2_arr) >= 2
        AND count($b1_arr_x) >= 1 AND count($b2_arr_x) >= 1
        AND in_array(35, $b1_arr_x) AND in_array(45, $b2_arr_x)
        // AND $interdental == 0
    ) {
        $first_val_3_2  = get_condition($TOOTH_NUMBERS_ISO[position_schema(end($b1_arr_x)) +1], $schema) == 'b' 
                            ? $TOOTH_NUMBERS_ISO[position_schema(end($b1_arr_x)) +2] : $TOOTH_NUMBERS_ISO[position_schema(end($b1_arr_x)) +1];
        
        $second_val_3_2 = get_condition($TOOTH_NUMBERS_ISO[position_schema($b2_arr_x[0])-1], $schema) == 'b'
                            ? $TOOTH_NUMBERS_ISO[position_schema($b2_arr_x[0])-2] : $TOOTH_NUMBERS_ISO[position_schema($b2_arr_x[0])-1];

        if((    !to_be_replaced(get_condition($first_val_3_2, $schema),
                    $first_val_3_2, $schema)
            AND 
            (get_condition($first_val_3_2, $schema) !== ')('
                AND get_condition($first_val_3_2, $schema) !== 'i' )
        )
        AND
        (   !to_be_replaced(get_condition($second_val_3_2, $schema),
                    $second_val_3_2, $schema)
            AND
            get_condition($second_val_3_2, $schema) !== ')('
        )
        // if(to_be_treated(get_condition($first_val_3_2, $schema))
        //     AND
        //     to_be_treated(get_condition($second_val_3_2, $schema))
        ) 
        {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $first_val_3_2.','.$second_val_3_2, "quantity"=> "2", "applied_rule"=> "BilateralFreeEnd_upper"]
            );
            if( veneering_grants($first_val_3_2) 
                AND !in_array($first_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $first_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 3"]
                );
            }
            if( veneering_grants($second_val_3_2) 
                AND !in_array($second_val_3_2, $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $second_val_3_2, "quantity"=> "1", "applied_rule"=> "veneering_grants 4"]
                );
            }
        }
    }
    // BILIt Second END

    //UNILAT START
    
    if(subsidy_exists(3.1)
        AND !in_array('bilat_upper', $uni_bi_array)
        AND ((count($q1_arr) >= 1
                AND count($q1_op_left_arr) >=1 
                AND in_array(15, $q1_op_left_arr_2) AND in_array(16, $q1_op_left_arr_2)
                AND (count($q2_op_right_arr) >= 2 OR count($q2_op_right_arr_2) >= 2)
                AND count($uni1_arr) >=3 AND checkConsec($uni1_arr)
            )
            OR
            (count($q2_arr) >= 1 
                AND count($q2_op_right_arr) >=1 
                AND in_array(25, $q2_op_right_arr_2) AND in_array(26, $q2_op_right_arr_2)
                AND (count($q1_op_left_arr) >= 2 OR count($q1_op_left_arr_2) >= 2)
                AND count($uni2_arr) >=3 AND checkConsec($uni2_arr)
            )
        )
    ) {
        // if((to_be_treated(get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema))
        //     OR get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema) == ''
        //     )
        //     AND
        //     (to_be_treated(get_condition($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema))
        //     OR get_condition($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema) == ''
        //     )
        // ) 
        if((    !to_be_replaced(get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema),
                    $TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema)
            AND 
            get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], $schema) !== ')('
        )
        AND
        (   !to_be_replaced(get_condition($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema),
                $TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema)
            AND
            get_condition($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], $schema) !== ')('
        )
        ) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1] .','. $TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], "quantity"=> "2", "applied_rule"=> "kollateral interdental gap_upper 1."]
            );
            if( veneering_grants($TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1]) ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $TOOTH_NUMBERS_ISO[position_schema(end($q1_op_left_arr)) +1], "quantity"=> "1", "applied_rule"=> "veneering_grants 7"]
                );
            }
            if( veneering_grants($TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1]) ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $TOOTH_NUMBERS_ISO[position_schema($q2_op_right_arr[0])-1], "quantity"=> "1", "applied_rule"=> "veneering_grants 8"]
                );
            }
        }
    }

    if(subsidy_exists(3.1)
        AND !in_array('bilat_lower', $uni_bi_array)
        AND ((count($q3_arr) >= 1
                AND count($q3_op_left_arr) >=1 
                AND in_array(35, $q3_op_left_arr_2) AND in_array(36, $q3_op_left_arr_2)
                AND (count($q4_op_right_arr) >= 2 OR count($q4_op_right_arr_2) >= 2 )
                AND count($uni3_arr) >=3 AND checkConsec($uni3_arr)
            )
            OR
            (count($q4_arr) >= 1 
                AND count($q4_op_right_arr) >=1 
                AND in_array(45, $q4_op_right_arr_2) AND in_array(46, $q4_op_right_arr_2)
                AND (count($q3_op_left_arr) >= 2 OR count($q3_op_left_arr_2) >= 2)
                AND count($uni4_arr) >=3 AND checkConsec($uni4_arr)
            )
        )
    ) {
        // if((to_be_treated(get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], $schema))
        //     )
        //     AND
        //     (to_be_treated(get_condition($TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], $schema))
        //     )
        // ) 
        if((    !to_be_replaced(get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], $schema),
                    $TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], $schema)
            AND 
            get_condition($TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], $schema) !== ')('
        )
        AND
        (   !to_be_replaced(get_condition($TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], $schema),
                $TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], $schema)
            AND
            get_condition($TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], $schema) !== ')('
        )
        ) {
            subsidy_remove_name('kollateral interdental gap');
            array_push($teeth_subsidy_eveluate,
                    ["subsidy"=> "3.2", "output_order"=> "8", "region"=> $TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1] .','. $TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], "quantity"=> "2", "applied_rule"=> "kollateral interdental gap_lower"]
            );

            if( veneering_grants($TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1]) ) {
                array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $TOOTH_NUMBERS_ISO[position_schema(end($q3_op_left_arr)) +1], "quantity"=> "1", "applied_rule"=> "veneering_grants 9"]
                );
            }

            if( veneering_grants($TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1]) ) {
                array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "4.7", "output_order"=> "9", "region"=> $TOOTH_NUMBERS_ISO[position_schema($q4_op_right_arr[0])-1], "quantity"=> "1", "applied_rule"=> "veneering_grants 9"]
                );
            }
        }
    }
    
    // check option 2.2
    if(//subsidy_exists('3.1') AND
        count($q1_arr_2x) == 2 AND count($q2_arr_2x) == 2
        AND count($interdental_arr) > 0
        AND checkConsec($interdental_arr)
        AND array_key_exists("0", $interdental_arr)
        // AND strpos(subsidy_exists_region(3.2)[0], strval($TOOTH_NUMBERS_ISO[position_schema($interdental_arr[0]) -1]) ) !== 0
        // AND isset(end($interdental_arr))
        // AND strpos(subsidy_exists_region(3.2)[0], strval($TOOTH_NUMBERS_ISO[position_schema(end($interdental_arr)) +1]) ) !== 3
    ) {
        if(count(subsidy_exists_region(3.2))> 0 )
        {
            if(strpos(subsidy_exists_region(3.2)[0], strval($TOOTH_NUMBERS_ISO[position_schema($interdental_arr[0]) -1]) ) === 0
                OR
                strpos(subsidy_exists_region(3.2)[0], strval($TOOTH_NUMBERS_ISO[position_schema(end($interdental_arr)) +1]) ) === 3)
            {
                return FALSE;
            }
        }

        if (to_be_replaced(get_condition(13, $schema), 13, $schema)
            AND to_be_replaced(get_condition(12, $schema), 12, $schema)
        ) {
            return FALSE;
        }

        if (to_be_replaced(get_condition(23, $schema), 23, $schema)
            AND to_be_replaced(get_condition(22, $schema), 22, $schema)
        ) {
            return FALSE;
        }

        if($interdental == 2) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $TOOTH_NUMBERS_ISO[position_schema($interdental_arr[0]) -1] .'-'. $TOOTH_NUMBERS_ISO[position_schema(end($interdental_arr)) +1], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBe Replaced 2"]
            );

            for($v= position_schema($interdental_arr[0]) -1; $v<= position_schema(end($interdental_arr)) +1; $v++) {
                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                ) {
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 1"]
                    );
                }
            }

            // veneering_grants_gap($is_gap_closure_arr, $schema);
        }

        if($interdental == 1) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[position_schema($interdental_arr[0]) -1] .'-'. $TOOTH_NUMBERS_ISO[position_schema(end($interdental_arr)) +1], "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapExactToBe Replaced 1"]
            );

            for($v= position_schema($interdental_arr[0]) -1; $v<= position_schema(end($interdental_arr)) +1; $v++) {
                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                ) {
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 2"]
                    );
                }
            }

            // veneering_grants_gap($is_gap_closure_arr, $schema);
        }
    }
}

function BilateralFreeEnd_mandible($schema, $teeth_with_status) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $teeth_with_status;

    // if (in_array('mandible_left_end', $teeth_region_eveluate) OR
    //     in_array('mandible_left_end', $teeth_region_eveluate)
    // ) {
    //     array_push($teeth_subsidy_eveluate, 
    //             ["subsidy"=> "3.1", "region"=> "upper_jaw_end", "applied_rule"=> "BilateralFreeEnd_upper"]
    //     );
    // }

    // if (in_array('mandible_left_end', $teeth_region_eveluate)
    // ) {
    //     array_push($teeth_subsidy_eveluate, 
    //             ["subsidy"=> "3.1", "region"=> "mandible_left_end", "applied_rule"=> "BilateralFreeEnd_upper"]
    //     );
    // }

    // $tooth_32_left = $teeth_with_status[0];
    // $tooth_32_right = end($teeth_with_status);

    // if ($tooth_32_left == 45 or $tooth_32_right == 35) {
    //     return True;
    // }

    // if (to_be_replaced($tooth_32_left) or $tooth_32_right->to_be_replaced) {
    //     return True;
    // }

    // array_push($teeth_subsidy_eveluate, 
    //     ["subsidy"=> "3.2", "region"=> "upper_jaw".$tooth_32_left.' '.$tooth_32_right, "applied_rule"=> "BilateralFreeEnd_upper"]
    // );
}

function special_ob_f($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $bs_separate_arr;
    
    $to_be_replaced_count = [];
    $to_separate_count = [];
    $separate_arr_temp = [];

    if(subsidy_exists(3.1)) {
        return FALSE;
    }

    foreach($schema as $key => $value) {
        
        if(to_be_replaced($value, $key, $schema) 
            AND (right($key, $schema)['status'] == '' 
                OR to_be_treated(right($key, $schema)['status'])
            )
            AND is_neighbor_gap_ob_f($key, $schema) == '1'
            AND !in_array($key, [18,28,38,48])
        ) {
            array_push($to_be_replaced_count, [$key]);
        }

        if(to_be_replaced($value, $key, $schema) 
            AND (to_be_replaced(right($key, $schema)['status'], $key, $schema)
                OR to_be_replaced(left($key, $schema)['status'], $key, $schema)
            )
            AND is_neighbor_gap_ob_f($key, $schema) == '0'
            AND !in_array($key, [18,28,38,48])
        ) {
            array_push($to_separate_count, $key);
        }

        if(count($to_separate_count) > 1
            AND !in_array($to_separate_count, $to_be_replaced_count)
        ) {
            array_push($to_be_replaced_count, $to_separate_count);
        }
    }

    $new_array = [];

    if(count($to_be_replaced_count) >= 3 ) {
        // for($be=0; $be<count($to_be_replaced_count); $be++) {
            $old_key = position_schema($to_be_replaced_count[0][0])-2;

            for($key = 0; $key < count($to_be_replaced_count); $key++ ) {
                $new_key = position_schema($to_be_replaced_count[$key][0]);

                if(($new_key - $old_key) == 2
                ) {
                    array_push($new_array, $to_be_replaced_count[$key][0]);

                    // $old_key += 2;
                    $old_key = position_schema(end($to_be_replaced_count[$key]));
                }
                else {
                    break;
                }
            }
        // }
    }

    $to_be_replaced_count = $new_array;


    if(count($to_be_replaced_count) >= 3 
        AND (get_jaw($to_be_replaced_count[0]) == get_jaw(end($to_be_replaced_count))) 
        //AND
        // ! (in_array('upper_jaw_left_end', $teeth_region_eveluate) OR
        // in_array('upper_jaw_right_end', $teeth_region_eveluate) OR
        // in_array('mandible_left_end', $teeth_region_eveluate) OR
        // in_array('mandible_right_end', $teeth_region_eveluate))
    ) {
        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw($to_be_replaced_count[0]), "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced=3"]
        );
    }

}

/** 3.x end **/

function special_case_b($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $TOOTH_NUMBERS_ISO, $is_gap_closure, $is_gap_closure_arr;
    $to_be_replaced_count   = [];
    $to_be_teated_count     = [];

    $right_b = 0;
    $left_b = 0;

    $bs_array = [];

    
    // if(subsidy_exists_name('BilateralFreeEnd') OR subsidy_exists_name('kollateral interdental gap')) {
    if(subsidy_exists(3.1) OR subsidy_exists_upper(3.1) OR
        subsidy_exists(4)
    ) {
        return FALSE;
    }

    foreach($schema as $teeth => $status) {
        if($status == 'b'
            // AND to_be_replaced($status, $teeth, $schema)
        ){
            if(to_be_replaced(right($teeth, $schema)['status'], $teeth, $schema)
                AND !in_array($teeth, [26,27,28,36,37,38])
                AND is_neighbor_gap($teeth, $schema) == '0'
                AND left($teeth, $schema)['status'] == 'b'
            ) {
                array_push($to_be_replaced_count, $teeth);
            }

            if(to_be_replaced(left($teeth, $schema)['status'], $teeth, $schema)
                AND !in_array($teeth, [18,17,16,48,47,46])
                AND is_neighbor_gap($teeth, $schema) == '0'
                AND right($teeth, $schema)['status'] == 'b'
            ) {
                array_push($to_be_replaced_count, $teeth);
            }

            if(to_be_treated(right($teeth, $schema)['status']) OR
                to_be_treated(left($teeth, $schema)['status']) OR
                right($teeth, $schema)['status'] == 'pw' OR
                left($teeth, $schema)['status'] == 'pw'
            ) {
                array_push($to_be_teated_count, $teeth);
            }

            if(right($teeth, $schema)['status'] == 'b') {
                array_push($bs_array, position_schema($teeth));
                $right_b++; //= right_b($teeth, $schema);
            }

            if(left($teeth, $schema)['status'] == 'b') {
                array_push($bs_array, position_schema($teeth));
                $left_b++; //= left_b($teeth, $schema);
            }
        }
    }

    $bs_array = array_unique($bs_array);
    sort($bs_array);    
    $bs = $right_b < $left_b ? $right_b : $left_b;

    /* NEED TO FIX LATER 
        ERROR wil be there if kbbk -spaces of two in between- kbbf
    */
    
    if(count($bs_array) > 0
        AND count($to_be_replaced_count) == 0
        AND count($to_be_teated_count) == 0
    ) {
        return false;
    }

    $right_count = 0;
    $left_count = 0;

    if($is_gap_closure == 'Y') {
        // if(checkConsec($teeth_with_status, $pos_sch, $pos_sch_right))
        {
            $gap_arr_bs = count($bs_array) > 0 ? $bs_array : [0];

            
            for($rc=0; $rc<count($is_gap_closure_arr); $rc++) {
                if(position_schema($is_gap_closure_arr[$rc]) < ($gap_arr_bs[0])) {
                    $left_count += 1;
                    $right_count += 1;
                }
                // if(position_schema($is_gap_closure_arr[$rc]) >= position_schema(end($bs_array))) {
                //     $right_count += 1;
                // }
            }
        }
    }

    if( count($to_be_replaced_count) > 0) {
        if(count($bs_array) > 0) {
            $pos_sch_right = end($bs_array) + $right_count + 1;
        }
        else {
            $pos_sch_right = position_schema(end($to_be_replaced_count)) + $right_count + 1;
        }

        $pos_sch       = position_schema($to_be_replaced_count[0]) + $left_count - 2;

        if( to_be_replaced(right(end($to_be_replaced_count), $schema)['status'], $teeth, $schema) ) {
            $pos_sch        = position_schema($to_be_replaced_count[0]) + $left_count - 1;
            $pos_sch_right  = position_schema(end($to_be_replaced_count)) + $right_count + 2;
        }
    }

    if(count($to_be_replaced_count) == 0 AND count($bs_array) > 0)
    {
        $pos_sch_right  = end($bs_array) + $right_count + 1;
        $pos_sch        = $bs_array[0] + $left_count - 1;
    }

    if(count($to_be_replaced_count) == 1) {
        if(to_be_replaced(right($to_be_replaced_count[0], $schema)['status'], $to_be_replaced_count[0], $schema))
        {
            $pos_sch        = position_schema($to_be_replaced_count[0]) + $left_count;
            $pos_sch_right  = position_schema(end($to_be_replaced_count)) + $right_count + 2;
        }


        if(to_be_replaced(left($to_be_replaced_count[0], $schema)['status'], $to_be_replaced_count[0], $schema))
        {
            $pos_sch        = position_schema($to_be_replaced_count[0]) + $left_count - 2;
            $pos_sch_right  = position_schema(end($to_be_replaced_count)) + $right_count;
        }
        

        array_push($teeth_subsidy_eveluate,
            ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapExactToBeReplaced1 bs"]
        );

        for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 3 bs"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }

    if(count($to_be_replaced_count) + $bs == 1) {
        array_push($teeth_subsidy_eveluate,
            ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapExactToBeReplaced2"]
        );

        for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 3"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }

    if(count($to_be_replaced_count) + $bs == 2
        // AND checkConsec($to_be_replaced_count)
    ) {
        array_push($teeth_subsidy_eveluate,
            ["subsidy"=> "2.3", "output_order"=> "11", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapExactToBeReplaced3"]
        );

        for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 4"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }

    if(count($to_be_teated_count) == 1 AND !subsidy_exists(2)) {
        if(count($bs_array) > 0) {
            $pos_sch_right = end($bs_array) + $right_count + 1;
        }
        else {
            $pos_sch_right = position_schema(end($to_be_teated_count)) + $right_count + 1;
        }

        $pos_sch       = position_schema($to_be_teated_count[0]) + $left_count - 1;

        array_push($teeth_subsidy_eveluate,
            ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced2"]
        );

        for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 5"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }
}

function right_bs($teeth, $schema) {
    $status = '';
    $b = 0;

    if(right($teeth, $schema)['status'] == 'b') {
        $b++;
        // right_b($teeth, $schema);
    }
    else {
        return ["status" => $status, "bs" => $b];
    }
    // print_r($b);
}

function left_bs($teeth, $schema) {
    $status = '';
    $b = 0;
    if(left($teeth, $schema)['status'] == 'b') {
        $b++;
        // left_b($teeth, $schema);
    }
    else {
        return ["status" => $status, "bs" => $b];
    }
}

function Exact2_3ToBeReplacedTeethInterdentalGapInFrontRegion($schema) {
    global $teeth_subsidy_eveluate, $teeth_region_eveluate, $TOOTH_NUMBERS_ISO, $is_gap_closure_arr, 
            $is_gap_closure, $teeth_subsidy_eveluate_2x, $is_optional_3x_2x;
    
    $to_be_replaced_count = [];

    if(subsidy_exists(3) OR subsidy_exists(4)
    ) {
        return FALSE;
    }

    if($is_optional_3x_2x == TRUE) {
        $schema[18] = '';
        $schema[17] = '';

        $schema[28] = '';
        $schema[27] = '';

        $schema[38] = '';
        $schema[37] = '';

        $schema[48] = '';
        $schema[47] = '';
    }

    foreach($schema as $key => $value) {
        
        // if(to_be_replaced($value, $key, $schema) AND right($key, $schema) AND to_be_replaced(right($key, $schema)['status'], $key, $schema)) {
        if(to_be_replaced($value, $key, $schema) AND (to_be_replaced(right($key, $schema)['status'], $key, $schema) 
                                                        OR right($key, $schema)['status'] == '' 
                                                        OR right($key, $schema)['status'] == ')('
                                                    ) 
                                                        // AND is_neighbor_gap($key, $schema) == ''
                                                        AND is_neighbor_gap($key, $schema) == '0'
                                                        AND !in_array($key, [18,28,38,48])
        ) {
            array_push($to_be_replaced_count, $key);
        }

        if(to_be_replaced($value, $key, $schema) AND (to_be_replaced(left($key, $schema)['status'], $key, $schema) 
                                                        OR left($key, $schema)['status'] == '' 
                                                        OR left($key, $schema)['status'] == ')(' 
                                                    ) 
                                                        // AND is_neighbor_gap($key, $schema) == ''
                                                        AND is_neighbor_gap($key, $schema) == '0'
                                                        AND !in_array($key, [18,28,38,48])
        ) {
            array_push($to_be_replaced_count, $key);
        }

        /*if(to_be_replaced($value, $key, $schema) AND (to_be_sw(left($key, $schema)['status'])
                                                        AND to_be_sw(right($key, $schema)['status'])
                                                    ) 
                                                        // AND is_neighbor_gap($key, $schema) == '0'
                                                        AND !in_array($key, [18,28,38,48])
        ) {
            array_push($to_be_replaced_count, left($key, $schema)['tooth']);
            array_push($to_be_replaced_count, $key);
            array_push($to_be_replaced_count, right($key, $schema)['tooth']);
        }*/
    }

    $to_be_replaced_count = array_values(array_unique($to_be_replaced_count));
    
    // var_dump($to_be_replaced_count);

    // $to_remove = array('18', '17', '28', '27', '38', '37', '48', '47');
    // if(count($to_be_replaced_count) > 2
    // ) {
    //     $to_be_replaced_count = array_diff($to_be_replaced_count, $to_remove);
    // }

    // $to_be_replaced_count = array_values(array_unique($to_be_replaced_count));

    if(! checkConsec($to_be_replaced_count)) {
        // return FALSE;
    }

    if(count($to_be_replaced_count) == 4 AND //!subsidy_exists(3.1) AND
        // !in_array('in_X7_X8', $teeth_region_eveluate) AND 
        (position_schema($to_be_replaced_count[2]) - position_schema($to_be_replaced_count[1]) > 2) 
    ) {
        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[0])-1] .'-'. $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[1])+1], "quantity"=> "1", "applied_rule"=> "BiggestInterdental GapExactToBeReplaced2"]
        );

        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[2])-1] .'-'. $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[3])+1], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced2"]
        );

        for($v= position_schema($to_be_replaced_count[0])-1; $v<= position_schema($to_be_replaced_count[1])+1; $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 6"]
                );
            }
        }

        for($v= position_schema($to_be_replaced_count[2])-1; $v<= position_schema($to_be_replaced_count[3])+1; $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 6"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }

    if(count($to_be_replaced_count) == 4 AND !subsidy_exists(3.1) AND
        ! in_array('in_X7_X8', $teeth_region_eveluate) AND 
        (position_schema($to_be_replaced_count[2]) - position_schema($to_be_replaced_count[1]) == 2) 
    ) {
        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw($to_be_replaced_count[0]), "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced 4 (!3.1)"]
        );

        return FALSE;
    }

    if(count($to_be_replaced_count) == 4 AND !subsidy_exists('3.1')
        AND checkConsec($to_be_replaced_count)
    ) {
        $atleastOneInPostRegion = FALSE;
        $inAnteriorRegion = FALSE;

        for($count=0; $count<count($to_be_replaced_count); $count++)
        {
            if( in_array($to_be_replaced_count[$count], [18,17,16,15,14,24,25,26,27,28,38,37,36,35,34,44,45,46,47,48])
            ) {
                $atleastOneInPostRegion = TRUE;
            }

            if( in_array($to_be_replaced_count[$count], [13,12,11,21,22,23,33,32,31,41,42,43])
            ) {
                $inAnteriorRegion = TRUE;
            }
        }

        if( $atleastOneInPostRegion == TRUE
            // AND $inAnteriorRegion == FALSE
        ) {
            array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "3.1", "output_order"=> "7", "region"=> get_jaw($to_be_replaced_count[0]), "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced--4"]
            );
        }
        else {
            array_push($teeth_subsidy_eveluate,
                    ["subsidy"=> "2.4", "output_order"=> "10", "region"=> $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[0])-1] .'-'. $TOOTH_NUMBERS_ISO[position_schema(end($to_be_replaced_count))+1], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced4"]
            );

            for($v= position_schema($to_be_replaced_count[0])-1; $v<= position_schema(end($to_be_replaced_count))+1; $v++) {
                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                ) {
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 7"]
                    );
                }
            }

            veneering_grants_gap($is_gap_closure_arr, $schema);
        }
    }

    if(count($to_be_replaced_count) == 3 //AND !subsidy_exists('2.4') 
        // AND
        // ! (in_array('upper_jaw_left_end', $teeth_region_eveluate) OR
        // in_array('upper_jaw_right_end', $teeth_region_eveluate) OR
        // in_array('mandible_left_end', $teeth_region_eveluate) OR
        // in_array('mandible_right_end', $teeth_region_eveluate))
    ) {
        $pos_sch = position_schema($to_be_replaced_count[0])-1;
        $pos_sch_right = position_schema(end($to_be_replaced_count))+1;

        if($is_gap_closure == 'Y') {
            $key = position_schema($to_be_replaced_count[0]);
            $gap_closure_arr_check = [];
            for($gaps= $pos_sch; $gaps<= $pos_sch_right; $gaps++)
            {
                if( in_array($TOOTH_NUMBERS_ISO[$gaps], $is_gap_closure_arr) )
                {
                    array_push($gap_closure_arr_check, $TOOTH_NUMBERS_ISO[$gaps]);
                }
            }
            {
                $right_count = 0;
                $left_count = 0;
                for($rc=0; $rc<count($gap_closure_arr_check); $rc++) {
                    if(position_schema($gap_closure_arr_check[$rc]) <= $key
                        AND  (position_schema($gap_closure_arr_check[$rc]) == $pos_sch AND 
                            position_schema($gap_closure_arr_check[$rc]) <= $pos_sch_right)
                    ) {
                        $left_count += 1;
                    }
                    if(position_schema($gap_closure_arr_check[$rc]) >= $key
                        AND  (position_schema($gap_closure_arr_check[$rc]) >= $pos_sch AND 
                            position_schema($gap_closure_arr_check[$rc]) == $pos_sch_right)
                    ) {
                        $right_count += 1;
                    }
                }
                $pos_sch -= $left_count;
                $pos_sch_right += $right_count;
            }
        }

        // $new_right_val = $TOOTH_NUMBERS_ISO[position_schema(end($to_be_replaced_count))+$value_add_right];
        // $new_left_val  = $TOOTH_NUMBERS_ISO[position_schema($to_be_replaced_count[0])-$value_add_left];

        array_push($teeth_subsidy_eveluate,
            ["subsidy"=> "2.3", "output_order"=> "11", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced 3"]
        );

        // for($v= position_schema($to_be_replaced_count[0])-$value_add_left; $v<= position_schema(end($to_be_replaced_count))+$value_add_right; $v++) {
        for($v= $pos_sch; $v<= $pos_sch_right; $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 8"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }

    if(count($to_be_replaced_count) == 2 AND !subsidy_exists('3.1') 
        // AND !in_array('in_X7_X8', $teeth_region_eveluate)
    ) {
        
        $pos_sch = position_schema($to_be_replaced_count[0])-1;
        $pos_sch_right = position_schema(end($to_be_replaced_count))+1;

        if($is_gap_closure == 'Y') {
            $key = position_schema($to_be_replaced_count[0]);
            $gap_closure_arr_check = [];
            for($gaps= $pos_sch; $gaps<= $pos_sch_right; $gaps++)
            {
                if( in_array($TOOTH_NUMBERS_ISO[$gaps], $is_gap_closure_arr) )
                {
                    array_push($gap_closure_arr_check, $TOOTH_NUMBERS_ISO[$gaps]);
                }
            }
            {
                $right_count = 0;
                $left_count = 0;
                for($rc=0; $rc<count($gap_closure_arr_check); $rc++) {
                    // var_dump($key .' '. position_schema($gap_closure_arr_check[$rc]) .' '.
                    //  $pos_sch .' '. $pos_sch_right);

                    if(position_schema($gap_closure_arr_check[$rc]) <= $key
                        AND  (position_schema($gap_closure_arr_check[$rc]) == $pos_sch AND 
                            position_schema($gap_closure_arr_check[$rc]) <= $pos_sch_right)
                    ) {
                        $left_count += 1;
                    }
                    if(position_schema($gap_closure_arr_check[$rc]) >= $key
                        AND  (position_schema($gap_closure_arr_check[$rc]) >= $pos_sch AND 
                            position_schema($gap_closure_arr_check[$rc]) == $pos_sch_right)
                    ) {
                        $right_count += 1;
                    }
                }
                $pos_sch -= $left_count;
                $pos_sch_right += $right_count;
            }
        }

        array_push($teeth_subsidy_eveluate,
                ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced-2 3"]
        );

        for($v= $pos_sch; $v<= $pos_sch_right; $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 9"]
                );
            }
        }

        veneering_grants_gap($is_gap_closure_arr, $schema);
    }
}

function Exact1ToBeReplacedTeethInterdentalGapInFrontRegion($schema) {
    global $teeth_subsidy_eveluate, $teeth_with_status, $TOOTH_NUMBERS_ISO, $is_gap_closure, 
            $is_gap_closure_arr, $is_optional_3x_2x;
    
    $to_be_replaced_count = [];
    $fs = [];
    $to_be_calculated = [];

    if(subsidy_exists_name('BilateralFreeEnd_upper')
        OR subsidy_exists_name('BiggestInterdentalGapExactToBeReplaced2_X7_X8')
    ) {
        // return FALSE; // ISSUEs
    }

    if(subsidy_exists(3.1) OR subsidy_exists(4)
    ) {
        return FALSE;
    }

    if($is_optional_3x_2x == TRUE) {
        $schema[18] = '';
        $schema[17] = '';

        $schema[28] = '';
        $schema[27] = '';

        $schema[38] = '';
        $schema[37] = '';

        $schema[48] = '';
        $schema[47] = '';
    }

    /** NEW Edit may change later */
    foreach($schema as $key => $value) {
        if((to_be_replaced($value, $key, $schema) OR $value == 'b')
            AND !in_array($key, [18, 28, 38, 48])
        ) {
            array_push($fs, $key);

            if(is_neighbor_gap($key, $schema) == 1)
            {
                array_push($to_be_calculated, $key);
            }
        }
    }
    /** NEW Edit may change later END Uncomment below 3 lines  */

    foreach($schema as $key => $value) {
        // if(to_be_replaced($value, $key, $schema)) {
        //     array_push($fs, $key);
        // }
        if(!in_array($key, [18, 28, 38, 48])
        ) {
            if((
                    (to_be_replaced($value, $key, $schema)
                    // OR $value == 'b'
                    )
                    AND 
                    (
                        right($key, $schema)['status'] == ''
                        OR right($key, $schema)['status'] == ')(' 
                        OR to_be_treated(right($key, $schema)['status'])
                        OR !to_be_replaced(right($key, $schema)['status'], $key, $schema)
                        // OR right($key, $schema)['status'] !== 'b'
                        // AND (!to_be_replaced(right(right($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                        //     AND right($key, $schema)['status'] == ')(')
                    )
                    AND 
                    ( 
                        left($key, $schema)['status'] == '' 
                        OR left($key, $schema)['status'] == ')(' 
                        OR to_be_treated(left($key, $schema)['status'])
                        OR !to_be_replaced(left($key, $schema)['status'], $key, $schema)
                        // OR left($key, $schema)['status'] !== 'b'
                        // AND (!to_be_replaced(left(left($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                        //     AND left($key, $schema)['status'] == ')(')
                    )
                )
                OR ($value == 'b'
                    AND to_be_treated(right($key, $schema)['status'])
                    AND to_be_treated(left($key, $schema)['status'])
                )
                // AND !in_array($key, [18, 28, 38, 48])
            ) {
                array_push($to_be_replaced_count, $key);
                // var_dump($key .' '. to_be_replaced(left($key, $schema)['status'], $key, $schema) 
                //     .' '. to_be_replaced(right($key, $schema)['status'], $key, $schema) . ' END');

                //start checking 3.1: 5_to_12 
                for($len=0; $len<count($teeth_subsidy_eveluate); $len++) {
                    if(( //$teeth_subsidy_eveluate[$len]['subsidy'] == '3.1' //OR
                        $teeth_subsidy_eveluate[$len]['applied_rule'] == 'Between5And12ToBeReplacedTeeth'
                        )
                    ) {
                        return FALSE;
                    }
                }
                //end checking 3.1: 5_to_12

                // f )( f : This will fo to 2.2 not 2.1
                if (
                    (//!to_be_replaced(right($key, $schema)['status'], $key, $schema)
                        //AND 
                        (to_be_replaced(right(right($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                        AND right($key, $schema)['status'] == ')(')
                    )
                    // OR
                    // (//!to_be_replaced(left($key, $schema)['status'], $key, $schema)
                    //     //AND 
                    //     (to_be_replaced(left(left($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                    //     AND left($key, $schema)['status'] == ')(')
                    // )
                ) {
                    return FALSE;
                }
                if (
                    // (//!to_be_replaced(right($key, $schema)['status'], $key, $schema)
                    //     //AND 
                    //     (to_be_replaced(right(right($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                    //     AND right($key, $schema)['status'] == ')(')
                    // )
                    // OR
                    (//!to_be_replaced(left($key, $schema)['status'], $key, $schema)
                        //AND 
                        (to_be_replaced(left(left($key, $schema)['tooth'], $schema)['status'], $key, $schema)
                        AND left($key, $schema)['status'] == ')(')
                    )
                ) {
                    return FALSE;
                }

                if(is_neighbor_gap($key, $schema) == 1 
                    // AND count($fs) > 1
                ) {
                    $pos_sch_right = position_schema($key) + 1;
                    $pos_sch       = position_schema($key) - 1;

                    if(to_be_treated(right($key, $schema)['status'])
                        OR right($key, $schema)['status'] == ')('
                    ) {
                        $pos_sch        = position_schema($key) - 1;
                        $pos_sch_right  = position_schema($key) + 1;
                    }
                    
                    if(count($to_be_calculated) == 2
                        AND (position_schema($to_be_calculated[1]) - position_schema($to_be_calculated[0])) == 2
                    ) {
                        $pos_sch_right = position_schema($to_be_calculated[0]) + 1;
                        $pos_sch       = position_schema($to_be_calculated[0]) - 1;

                        if(!in_array($TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], subsidy_exists_region(2.1)) 
                        ) {
                            /*if( (in_array($TOOTH_NUMBERS_ISO[$pos_sch], [17])
                                    AND !to_be_replaced(get_condition(18, $schema), 18, $schema))
                                AND
                                (in_array($TOOTH_NUMBERS_ISO[$pos_sch], [47])
                                    AND !to_be_replaced(get_condition(48, $schema), 48, $schema))
                            ) {
                                array_push($teeth_subsidy_eveluate, 
                                    ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced_2.X"]
                                );

                                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                                    ) {
                                        array_push($teeth_subsidy_eveluate, 
                                            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 11"]
                                        );
                                    }
                                }

                                veneering_grants_gap($is_gap_closure_arr, $schema);
                            }*/                            
                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced_2.X"]
                            );

                            for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                                ) {
                                    array_push($teeth_subsidy_eveluate, 
                                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 11"]
                                    );
                                }
                            }

                            veneering_grants_gap($is_gap_closure_arr, $schema);
                        }

                        if( ! subsidy_exists(2.5)
                            //AND count($fs)>1
                            // get_jaw($fs[0]) !== get_jaw(end($fs))
                        ) {
                            $pos_sch_right = position_schema($to_be_calculated[1]) + 1;
                            $pos_sch       = position_schema($to_be_calculated[1]) - 1;

                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.5", "output_order"=> "14", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "2.X_with_neighbor"]
                            );
                            
                            for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                                ) {
                                    array_push($teeth_subsidy_eveluate, 
                                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 10"]
                                    );
                                }
                            }
                        }
                    }
                    else {
                        // var_dump(is_neighbor_gap($key, $schema));
                        // var_dump(($key));

                        if( !subsidy_exists(2.5) AND subsidy_exists(2)
                            AND !subsidy_exists(2.4)
                            //AND count($fs)>1
                            // get_jaw($fs[0]) !== get_jaw(end($fs))
                        ) {
                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.5", "output_order"=> "14", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "2.X_with_neighbor"]
                            );
                            
                            for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                                if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                                    AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                                ) {
                                    array_push($teeth_subsidy_eveluate, 
                                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 10"]
                                    );
                                }
                            }
                        }
                        else { // case: 38F, 36F
                            $pos_sch_right = position_schema($key) + 1;
                            $pos_sch       = position_schema($key) - 1;

                            // if(!subsidy_exists_name('BiggestInterdentalGapExactToBeReplaced_2.X'))
                            {
                                array_push($teeth_subsidy_eveluate, 
                                    ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced_2.X 1"]
                                );

                                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                                    ) {
                                        array_push($teeth_subsidy_eveluate, 
                                            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 11 2.x"]
                                        );
                                    }
                                }

                                veneering_grants_gap($is_gap_closure_arr, $schema);
                            }
                        }
                    }

                    order_check_2_5($schema);

                    // if(count($teeth_with_status) == 3)
                    // {
                    //     subsidy_remove_name('2.X_with_neighbor');
                    //     array_push($teeth_subsidy_eveluate, 
                    //         ["subsidy"=> "3.1", "output_order"=> "7", "region"=> "biggest_interdental_gap", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced 1"]
                    //     );
                    // }
                    // return FALSE;
                }
                else {
                    $pos_sch_right = position_schema($key) + 1;
                    $pos_sch       = position_schema($key) - 1;

                    if(to_be_treated(right($key, $schema)['status'])
                        OR right($key, $schema)['status'] == ')('
                    ) {
                        $pos_sch        = position_schema($key) - 1;
                        $pos_sch_right  = position_schema($key) + 1;
                    }

                    if(left($key, $schema)['status'] == ')('
                    ) {
                        $pos_sch        = position_schema($key) - 1;
                        $pos_sch_right  = position_schema($key) + 1;
                    }
                    
                    if($is_gap_closure == 'Y') {
                        // var_dump(checkConsec($teeth_with_status));
                        // if(checkConsec($teeth_with_status))
                        {
                            $right_count = 0;
                            $left_count = 0;
                            for($rc=0; $rc<count($is_gap_closure_arr); $rc++) {
                                // var_dump(position_schema($is_gap_closure_arr[$rc]) .' '.
                                //  $pos_sch .' '. $pos_sch_right);

                                if(position_schema($is_gap_closure_arr[$rc]) <= position_schema($key)
                                //position_schema($teeth_with_status[0])
                                    AND  (position_schema($is_gap_closure_arr[$rc]) >= $pos_sch AND 
                                        position_schema($is_gap_closure_arr[$rc]) <= $pos_sch_right)
                                ) {
                                    $left_count += 1;
                                }
                                if(position_schema($is_gap_closure_arr[$rc]) >= position_schema($key)
                                //position_schema(end($teeth_with_status))
                                    AND  (position_schema($is_gap_closure_arr[$rc]) >= $pos_sch AND 
                                        position_schema($is_gap_closure_arr[$rc]) <= $pos_sch_right)
                                ) {
                                    $right_count += 1;
                                }
                            }
                            $pos_sch -= $left_count;
                            $pos_sch_right += $right_count;
                        }
                    }
                    
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced-1"]
                    );

                    for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                        if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                            AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                        ) {
                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                            );
                        }

                        // if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                        //     AND in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                        // ) {
                        //     array_push($teeth_subsidy_eveluate, 
                        //         ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                        //     );
                        
                        //     array_push($teeth_subsidy_eveluate, 
                        //         ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                        //     );
                        // }
                    }

                    veneering_grants_gap($is_gap_closure_arr, $schema);
                }
            }
        }
    }
    // insert into subsidy array outside the array
}

function order_check_2_5($schema)
{
    if (! subsidy_exists(2.5))
    {
        return FALSE;
    }

    $val25 = subsidy_exists_region_2x(2.5)[0];
    
    $val21 = subsidy_exists_region_2x(2.1);
}

function checkConsec($d) {
    global $TOOTH_NUMBERS_ISO, $is_gap_closure_arr, $teeth_subsidy_eveluate, $teeth_with_status;
    $return_array = [];

    for($i=0;$i<count($d);$i++) {
        $d[$i] = position_schema($d[$i]);
    }
    for($i=0;$i<count($d);$i++) {
        if(isset($d[$i+1]) && 
            ($d[$i]+1) != ($d[$i+1])
        ) {
            return false;
        }
    }
    return true;
}

function veneering_grants_gap($is_gap_closure_arr, $schema)
{
    global $teeth_subsidy_eveluate;

    if((in_array(15, $is_gap_closure_arr) OR in_array(14, $is_gap_closure_arr) OR in_array(13, $is_gap_closure_arr)
            OR in_array(12, $is_gap_closure_arr) OR in_array(11, $is_gap_closure_arr))
        AND in_array(16, array_merge(
            subsidy_exists_region_2x(2.1),
            subsidy_exists_region_2x(2.2),
            subsidy_exists_region_2x(2.3),
            subsidy_exists_region_2x(2.4),
            subsidy_exists_region_2x(2.5)
        ))
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 16, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(25, $is_gap_closure_arr) OR in_array(24, $is_gap_closure_arr) OR in_array(23, $is_gap_closure_arr)
            OR in_array(22, $is_gap_closure_arr) OR in_array(21, $is_gap_closure_arr))
        AND in_array(26, array_merge(
            subsidy_exists_region_2x(2.1),
            subsidy_exists_region_2x(2.2),
            subsidy_exists_region_2x(2.3),
            subsidy_exists_region_2x(2.4),
            subsidy_exists_region_2x(2.5)
        ))
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 26, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(34, $is_gap_closure_arr) OR in_array(33, $is_gap_closure_arr)
            OR in_array(32, $is_gap_closure_arr) OR in_array(31, $is_gap_closure_arr))
        AND in_array(35, array_merge(
            subsidy_exists_region_2x(2.1),
            subsidy_exists_region_2x(2.2),
            subsidy_exists_region_2x(2.3),
            subsidy_exists_region_2x(2.4),
            subsidy_exists_region_2x(2.5)
        ))
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 35, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(44, $is_gap_closure_arr) OR in_array(43, $is_gap_closure_arr)
            OR in_array(42, $is_gap_closure_arr) OR in_array(41, $is_gap_closure_arr))
        AND in_array(45, array_merge(
            subsidy_exists_region_2x(2.1),
            subsidy_exists_region_2x(2.2),
            subsidy_exists_region_2x(2.3),
            subsidy_exists_region_2x(2.4),
            subsidy_exists_region_2x(2.5)
        ))
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 45, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

}

function veneering_grants_gap__($is_gap_closure_arr, $schema)
{
    global $teeth_subsidy_eveluate;

    if((in_array(15, $is_gap_closure_arr) OR in_array(14, $is_gap_closure_arr) OR in_array(13, $is_gap_closure_arr)
        OR in_array(12, $is_gap_closure_arr) OR in_array(11, $is_gap_closure_arr))
        AND (
                (get_condition(16, $schema) == '' 
                AND ( get_condition(15, $schema) == ')(' 
                    OR to_be_replaced(get_condition(15, $schema), 15, $schema)
                )
                AND (to_be_treated_replaced(get_condition(14, $schema))
                        AND get_condition(14, $schema) !== ')(')
                )
            OR
                (to_be_treated(get_condition(16, $schema))
                AND (to_be_replaced(get_condition(15, $schema), 15, $schema)
                    OR to_be_replaced(get_condition(14, $schema), 14, $schema))
                )
            OR
                to_be_replaced(get_condition(16, $schema), 16, $schema)
            OR
                (get_condition(16, $schema) !== ''
                AND (!to_be_treated(get_condition(16, $schema))
                    AND (to_be_treated_replaced(get_condition(15, $schema)) 
                        AND get_condition(15, $schema) !== ')(')
                    )
                AND (!to_be_replaced(get_condition(16, $schema), 16, $schema)
                    OR to_be_replaced(get_condition(16, $schema), 16, $schema)
                    )
                )
            )
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 16, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(25, $is_gap_closure_arr) OR in_array(24, $is_gap_closure_arr) OR in_array(23, $is_gap_closure_arr)
        OR in_array(22, $is_gap_closure_arr) OR in_array(21, $is_gap_closure_arr))
        AND (
            (get_condition(26, $schema) == '' 
            AND ( get_condition(25, $schema) == ')(' 
                OR to_be_replaced(get_condition(25, $schema), 25, $schema)
            )
            // AND (to_be_treated_replaced(get_condition(24, $schema))
            //         AND get_condition(24, $schema) !== ')(')
            )
        OR
            (to_be_treated(get_condition(26, $schema))
            AND (to_be_replaced(get_condition(25, $schema), 25, $schema)
                OR to_be_replaced(get_condition(24, $schema), 24, $schema))
            )
        OR
            to_be_replaced(get_condition(26, $schema), 26, $schema)
        OR
            (get_condition(26, $schema) !== ''
            AND (!to_be_treated(get_condition(26, $schema))
                AND (to_be_treated_replaced(get_condition(25, $schema))
                    AND get_condition(25, $schema) !== ')(')
                )
            AND (!to_be_replaced(get_condition(26, $schema), 26, $schema)
                OR to_be_replaced(get_condition(26, $schema), 26, $schema)
                )
            )
        )
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 26, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(34, $is_gap_closure_arr) OR in_array(33, $is_gap_closure_arr)
        OR in_array(32, $is_gap_closure_arr) OR in_array(31, $is_gap_closure_arr))
        AND (
            (get_condition(35, $schema) == '' 
            AND ( get_condition(34, $schema) == ')(' 
                OR to_be_replaced(get_condition(34, $schema), 34, $schema)
            )
            AND (to_be_treated_replaced(get_condition(33, $schema))
                    AND get_condition(33, $schema) !== ')(')
            )
        OR
            (to_be_treated(get_condition(35, $schema))
            AND (to_be_replaced(get_condition(34, $schema), 34, $schema)
                OR to_be_replaced(get_condition(33, $schema), 33, $schema))
            )
        OR
            to_be_replaced(get_condition(36, $schema), 36, $schema) // ISSUE should be 35
        OR
            (get_condition(35, $schema) !== ''
            AND (!to_be_treated(get_condition(35, $schema))
                AND (to_be_treated_replaced(get_condition(34, $schema)))
                    AND get_condition(34, $schema) !== ')('
                )
            AND (!to_be_replaced(get_condition(35, $schema), 35, $schema)
                OR to_be_replaced(get_condition(35, $schema), 35, $schema)
                )
            )
        )
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 35, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }

    if((in_array(44, $is_gap_closure_arr) OR in_array(43, $is_gap_closure_arr)
        OR in_array(42, $is_gap_closure_arr) OR in_array(41, $is_gap_closure_arr))
        AND (
            (get_condition(45, $schema) == '' 
            AND ( get_condition(44, $schema) == ')(' 
                OR to_be_replaced(get_condition(44, $schema), 44, $schema)
            )
            AND (to_be_treated_replaced(get_condition(43, $schema))
                    AND get_condition(43, $schema) !== ')(')
            )
        OR
            (to_be_treated(get_condition(45, $schema))
            AND (to_be_replaced(get_condition(44, $schema), 44, $schema)
                OR to_be_replaced(get_condition(43, $schema), 43, $schema))
            )
        OR
            to_be_replaced(get_condition(45, $schema), 45, $schema)
        OR
            (get_condition(45, $schema) !== ''
            AND (!to_be_treated(get_condition(45, $schema))
                AND (to_be_treated_replaced(get_condition(44, $schema))
                    AND get_condition(44, $schema) !== ')('
                )
            )
            AND (!to_be_replaced(get_condition(45, $schema), 45, $schema)
                OR to_be_replaced(get_condition(45, $schema), 45, $schema)
                )
            )
        )
    ) {
        array_push($teeth_subsidy_eveluate, 
            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> 45, "quantity"=> "1", "applied_rule"=> "Veneering grants g1"]
        );
    }
}

/** 7.x start */
function sw_7_2($schema) {
    global $teeth_with_status, $teeth_subsidy_eveluate, $teeth_inter_dental_gaps, $TOOTH_NUMBERS_ISO, 
            $is_gap_closure_arr, $is_gap_closure, $teeth_subsidy_eveluate_2x, $is_optional_3x_2x,
            $is_optional_3x_1x;

    $teeth_included = [];
    $get_jaw = '';
    $is_next_tbr = '';

    if(subsidy_exists_name('X7_X8_')
        AND !to_be_replaced(get_condition(16, $schema), 16, $schema)
        AND !to_be_replaced(get_condition(26, $schema), 26, $schema)
        AND !to_be_replaced(get_condition(36, $schema), 36, $schema)
        AND !to_be_replaced(get_condition(46, $schema), 46, $schema)
    ) {
        // $teeth_subsidy_eveluate = [];
        subsidy_remove_name('X7_X8_');
    }

    if(subsidy_exists(4.6)
        OR subsidy_exists(3) OR subsidy_exists_optional(3)
    ) {
        return FALSE;
    }
    // var_dump($teeth_subsidy_eveluate);
    // var_dump($teeth_subsidy_eveluate_2x);
    
    foreach($schema as $tooth => $value) {
        if( to_be_sw($value) OR $value == "swk"
        ) {
            if( $value !== ')(' 
                AND
                (right($tooth, $schema)['status'] == 'sw' OR 
                    left($tooth, $schema)['status'] == 'sw')
                AND
                (right($tooth, $schema)['status'] !== 'swb' AND 
                    left($tooth, $schema)['status'] !== 'swb')
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }

                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if( $value !== ')(' 
                AND
                (right($tooth, $schema)['status'] == 'swb' OR 
                    left($tooth, $schema)['status'] == 'swb')
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if( (
                (to_be_replaced(right($tooth, $schema)['status'], right($tooth, $schema)['tooth'], $schema)
                    AND
                    (right($tooth, $schema)['tooth'] == '28' 
                        OR right($tooth, $schema)['tooth'] == '48'
                    )
                )
                OR
                (to_be_replaced(left($tooth, $schema)['status'], left($tooth, $schema)['tooth'], $schema)
                    AND
                    (left($tooth, $schema)['tooth'] == '18' 
                        OR left($tooth, $schema)['tooth'] == '38'
                    )
                )
                )
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch-1]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch-1], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            /*else if( $value !== ')(' AND $value == 'swb'
                AND
                left($tooth, $schema)['status'] == 'sw'
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch+1]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch+1], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }*/
        }

        if( $value == 'swb'
        ) {
            if( $value !== ')(' 
                // AND
                // (right($tooth, $schema)['status'] == 'swb' OR 
                //     left($tooth, $schema)['status'] == 'swb')
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if( $value !== ')('
                AND
                (right($tooth, $schema)['status'] == 'sw'
                OR right($tooth, $schema)['status'] == 'swk')
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if( $value !== ')('
                AND
                (left($tooth, $schema)['status'] == 'sw'
                OR left($tooth, $schema)['status'] == 'swk')
                AND !in_array($tooth, array_merge(
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                ))
            ) {
                $pos_sch = position_schema($tooth);

                // if(subsidy_exists_name('X7_X8_')
                // ) {
                //     $teeth_subsidy_eveluate = [];
                // }

                if( subsidy_exists(3) OR subsidy_exists_optional(3)
                ) {
                    if(subsidy_exists_name('X7_X8_')
                    ) {
                        $teeth_subsidy_eveluate = [];
                    }
                    else {
                        return FALSE;
                    }
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.2", "output_order"=> "9.8", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
        }
    }

}

function sw_7_1($schema) {
    global $teeth_with_status, $teeth_subsidy_eveluate, $teeth_inter_dental_gaps, $TOOTH_NUMBERS_ISO, 
            $is_gap_closure_arr, $is_gap_closure;

    $teeth_included = [];
    $get_jaw = '';
    $is_next_tbr = '';

    if(subsidy_exists(4.6) 
        OR subsidy_exists(3) OR subsidy_exists_optional(3)
    ) {
        return FALSE;
    }

    foreach($schema as $tooth => $value) {
        if( to_be_sw($value)
        ) {
            if( $value !== ')(' 
                AND (
                    (right($tooth, $schema)['status'] == '' AND left($tooth, $schema)['status'] == '') 
                    OR
                    (to_be_treated(left($tooth, $schema)['status']) OR left($tooth, $schema)['status'] == 'pw')
                    ) 
                AND
                    !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                AND !in_array($tooth, array_merge(
                    subsidy_exists_region(4.7), 
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
            ) {
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    // return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if(  $value !== ')(' 
                AND ((right($tooth, $schema)['status'] == '' AND left($tooth, $schema)['status'] == '') OR
                (to_be_treated(right($tooth, $schema)['status']) OR right($tooth, $schema)['status'] == 'pw')) AND
                !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema)
                AND !in_array($tooth, array_merge(
                    subsidy_exists_region(4.7),
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    // return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2d"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND 
                    (!to_be_treated(right($tooth, $schema)['status'])
                        AND !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (!to_be_treated(left($tooth, $schema)['status'])
                        AND !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) { 
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    // return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2c"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND
                    in_array($tooth, [17,27,37,47,16,46])
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                    AND
                    (//!to_be_treated(right($tooth, $schema)['status'])
                         !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (//!to_be_treated(left($tooth, $schema)['status'])
                        to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    // return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2b"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND in_array($tooth, [17,27,37,47,26,36])
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                    AND
                    (!to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
            ) {
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    // return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2b"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                AND subsidy_exists(3.1)
            ) {
                if(!in_array($tooth, array_merge(
                    subsidy_exists_region(4.7), 
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                ) {
                    $pos_sch = position_schema($tooth);

                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "7.1", "output_order"=> "9.9", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2a"]
                    );
                    if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    ){
                        array_push($teeth_subsidy_eveluate, 
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                        );
                    }
                }
            }
        }
    }
}

/** 7.x end */

/** 1.x start **/
function StatusPwInPosteriorRegion($schema) {
    global $teeth_subsidy_eveluate, $TOOTH_NUMBERS_ISO;

    // pw in 18-14, 24-28 (posterior region)

    $StatusPwInPosteriorRegion = [];

    foreach($schema as $teeth => $status) {
        if (($teeth == 18 OR $teeth == 17 OR $teeth == 16 OR $teeth == 15 OR $teeth == 14 OR
            $teeth == 24 OR $teeth == 25 OR $teeth == 26 OR $teeth == 27 OR $teeth == 28 OR
            $teeth == 38 OR $teeth == 37 OR $teeth == 36 OR $teeth == 35 OR $teeth == 34 OR
            $teeth == 44 OR $teeth == 45 OR $teeth == 46 OR $teeth == 47 OR $teeth == 48 ) AND
            !to_be_replaced($status, $teeth, $schema) AND
            // (!to_be_replaced(right($teeth, $schema)['status'], $teeth, $schema) AND
            //  !to_be_replaced(left($teeth, $schema)['status'], $teeth, $schema))
            !in_array($teeth, array_merge(
                subsidy_exists_region(4.7), 
                subsidy_exists_region_2x(2.1),
                subsidy_exists_region_2x(2.2),
                subsidy_exists_region_2x(2.3),
                subsidy_exists_region_2x(2.4)
                ))
        ) {
            // array_push($StatusPwInPosteriorRegion, get_condition($teeth, $schema));
            if(get_condition($teeth, $schema) == 'pw')
                array_push($StatusPwInPosteriorRegion, position_schema($teeth));
        }
    }

    // if (!subsidy_exists(2.1))
    {
        for($r=0; $r<count($StatusPwInPosteriorRegion); $r++ ) {
            array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.2", "output_order"=> "18", "region"=> $TOOTH_NUMBERS_ISO[$StatusPwInPosteriorRegion[$r]].'', "quantity"=> "1", "applied_rule"=> "StatusPwInPosteriorRegion"]
            );
        }
    }
}

function StatusPwInFrontRegion($schema) {
    global $teeth_subsidy_eveluate;

    // pw in Region(13, 23, $this->schema), Region(33, 43, $this->schema) (posterior region)

    $StatusPwInFrontRegion = [];
    $region = [];

    foreach($schema as $teeth => $status) {
        if (($teeth == 13 OR $teeth == 12 OR $teeth == 11 OR $teeth == 21 OR $teeth == 22 OR $teeth == 23 OR
            $teeth == 33 OR $teeth == 32 OR $teeth == 31 OR $teeth == 41 OR $teeth == 42 OR $teeth == 43)
            // AND !to_be_replaced(right($teeth, $schema)['status'], $teeth, $schema) 
            // AND !to_be_replaced(left($teeth, $schema)['status'], $teeth, $schema)
            AND !in_array($teeth, array_merge(
                subsidy_exists_region(4.7), 
                subsidy_exists_region_2x(2.1),
                subsidy_exists_region_2x(2.2),
                subsidy_exists_region_2x(2.3),
                subsidy_exists_region_2x(2.4)
                ))
        ) {
            // array_push($StatusPwInFrontRegion, get_condition($teeth, $schema));
            if(get_condition($teeth, $schema) == 'pw')
                array_push($StatusPwInFrontRegion, $teeth);
        }
    }


    // if (in_array('pw', $StatusPwInFrontRegion) AND !subsidy_exists(2.2)) {
    for($r=0; $r<count($StatusPwInFrontRegion); $r++ ) {
        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $StatusPwInFrontRegion[$r] .'' , "quantity"=> "1", "applied_rule"=> "StatusPwInFrontRegion"]
        );
        if( veneering_grants($StatusPwInFrontRegion[$r]) ){
            array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $StatusPwInFrontRegion[$r], "quantity"=> "1", "applied_rule"=> "veneering_grants 12"]
            );
        }
    }
}

function ToBeTreatedWithNoAbutmentTeethIncluded($schema) {
    global $teeth_with_status, $teeth_subsidy_eveluate, $teeth_inter_dental_gaps, $TOOTH_NUMBERS_ISO, 
        $is_gap_closure_arr, $is_gap_closure, $is_optional_3x_2x, $teeth_subsidy_eveluate_2x;

    $teeth_included = [];
    $get_jaw = '';
    $is_next_tbr = '';

    if(subsidy_exists(4.6)
    ) {
        return FALSE;
    }

    /* to check neighbouring interdental gap : todo */
    foreach($teeth_inter_dental_gaps as $start => $end) {
        array_push($teeth_included, intval($start));
        array_push($teeth_included, intval($end));
    }
    
    foreach($schema as $tooth => $value) {
        if( (to_be_treated($value) OR $value == ')(' OR $value == 'swk')
            AND $value !== 'pw'
        ) {
            if(
            (   ((to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema) 
                    OR right($tooth, $schema)['status'] == 'b')
                    AND !in_array($tooth, [27,37])
                )
                AND //OR 
                ((to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema)
                    OR left($tooth, $schema)['status'] == 'b')
                    AND !in_array($tooth, [17,47])
                )
             ) 
             AND
             ((right(right($tooth, $schema)['tooth'], $schema)['status'] == '' AND
             left(left($tooth, $schema)['tooth'], $schema)['status'] == '')
             OR (right(right($tooth, $schema)['tooth'], $schema)['status'] == 'k' OR
                left(left($tooth, $schema)['tooth'], $schema)['status'] == 'k'
             ))
             AND !subsidy_exists_name('BiggestInterdentalGapExactToBeReplaced=3')
             AND !subsidy_exists(2.1)
             AND !subsidy_exists(3.1)
            //  AND !subsidy_exists(2.2)
             AND (!to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                    AND !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
            ) {
                $get_jaw = get_jaw($tooth);
                // if(subsidy_exists(2.1))
                // {
                    // return FALSE;
                // }

                $pos_sch_right = position_schema($tooth);
                $pos_sch       = position_schema($tooth) - 2;

                if(to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                    OR right($tooth, $schema)['status'] == 'b'
                ) {
                    $pos_sch        = position_schema($tooth);
                    $pos_sch_right  = position_schema($tooth) + 2;
                }

                if($is_gap_closure == 'Y') {
                    $right_count = 0;
                    $left_count = 0;
                    
                    for($rc=0; $rc<count($is_gap_closure_arr); $rc++) {
                        if(position_schema($is_gap_closure_arr[$rc]) <= position_schema($tooth)
                            AND  (position_schema($is_gap_closure_arr[$rc]) >= $pos_sch AND 
                                position_schema($is_gap_closure_arr[$rc]) <= $pos_sch_right)
                        ) {
                            $left_count += 1;
                        }
                        if(position_schema($is_gap_closure_arr[$rc]) >= position_schema($tooth)
                            AND  (position_schema($is_gap_closure_arr[$rc]) >= $pos_sch AND 
                                position_schema($is_gap_closure_arr[$rc]) <= $pos_sch_right)
                        ) {
                            $right_count += 1;
                        }
                    }

                    $pos_sch -= $left_count;
                    $pos_sch_right += $right_count;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "TBT_next_to_TBR 1"]
                );

                $gap_count_selected     = 0;
                $gap_count_selected_arr = [];

                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                    if(in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        $gap_count_selected =+ 1;
                    }
                    if(!veneering_grants($TOOTH_NUMBERS_ISO[$v])
                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        array_push($gap_count_selected_arr, $TOOTH_NUMBERS_ISO[$v]);
                    }
                }

                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 13"]
                        );
                    }

                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                            AND in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                        ) {
                            // if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$pos_sch], $schema))
                            // ) 
                            {
                                array_push($teeth_subsidy_eveluate, 
                                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                                );
                            }

                            // if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$pos_sch_right], $schema))
                            // ) 
                            {
                                array_push($teeth_subsidy_eveluate, 
                                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                                );
                            }
                        }
                }
            }
            else if(
                (   ((to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema) 
                    OR right($tooth, $schema)['status'] == 'b')
                        AND !in_array($tooth, [27,37])
                    )
                    AND //OR 
                    ((to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema)
                        OR left($tooth, $schema)['status'] == 'b')
                        AND !in_array($tooth, [17,47])
                    )
                )
                AND
                ((right(right($tooth, $schema)['tooth'], $schema)['status'] == '' AND
                left(left($tooth, $schema)['tooth'], $schema)['status'] == '')
                OR (right(right($tooth, $schema)['tooth'], $schema)['status'] == 'k' OR
                left(left($tooth, $schema)['tooth'], $schema)['status'] == 'k'
                ))
                AND !subsidy_exists_name('BiggestInterdentalGapExactToBeReplaced=3')
                // AND !subsidy_exists(2.1)
                AND !subsidy_exists(3.1)
                // AND !subsidy_exists(2.2)
                AND !in_array($tooth, array_merge(
                    subsidy_exists_region(4.7), 
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                // AND (!to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                //     AND !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
            ) {
                $get_jaw = get_jaw($tooth);
                // if(subsidy_exists(2.1))
                // {
                    // return FALSE;
                // }

                $pos_sch_right = position_schema($tooth);
                $pos_sch       = position_schema($tooth) - 2;

                if(to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                    OR right($tooth, $schema)['status'] == 'b'
                ) {
                    $pos_sch        = position_schema($tooth);
                    $pos_sch_right  = position_schema($tooth) + 2;
                }

                if($is_gap_closure == 'Y') {
                    $right_count = 0;
                    $left_count = 0;
                    for($rc=0; $rc<count($is_gap_closure_arr); $rc++) {
                        if(position_schema($is_gap_closure_arr[$rc]) <= position_schema($tooth)
                            AND  (position_schema($is_gap_closure_arr[$rc]) == $pos_sch AND 
                                position_schema($is_gap_closure_arr[$rc]) <= $pos_sch_right)
                        ) {
                            $left_count += 1;
                        }
                        if(position_schema($is_gap_closure_arr[$rc]) >= position_schema($tooth)
                            AND  (position_schema($is_gap_closure_arr[$rc]) >= $pos_sch AND 
                                position_schema($is_gap_closure_arr[$rc]) == $pos_sch_right)
                        ) {
                            $right_count += 1;
                        }
                    }

                    $pos_sch -= $left_count;
                    $pos_sch_right += $right_count;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.1", "output_order"=> "13", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch] .'-'. $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "TBT_next_to_TBR 2"]
                );

                $gap_count_selected     = 0;
                $gap_count_selected_arr = [];

                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                    if(in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        $gap_count_selected =+ 1;
                    }
                    if(!veneering_grants($TOOTH_NUMBERS_ISO[$v])
                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        array_push($gap_count_selected_arr, $TOOTH_NUMBERS_ISO[$v]);
                    }
                }

                for($v= ($pos_sch); $v<= ($pos_sch_right); $v++) {
                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                        AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        array_push($teeth_subsidy_eveluate, 
                            ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 13"]
                        );
                    }

                    if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                            AND in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
                    ) {
                        // if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$pos_sch], $schema))
                        // ) 
                        {
                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                            );
                        }

                        if(!to_be_treated(get_condition($TOOTH_NUMBERS_ISO[$pos_sch_right], $schema))
                        ) {
                            array_push($teeth_subsidy_eveluate, 
                                ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch_right], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                            );
                        }
                    }
                }
            }
            else if( $value !== ')(' 
                AND (
                    (right($tooth, $schema)['status'] == '' AND left($tooth, $schema)['status'] == '') 
                    OR
                    (to_be_treated(left($tooth, $schema)['status']) OR left($tooth, $schema)['status'] == 'pw')
                    ) 
                AND
                    !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema)
                AND !in_array($tooth, array_merge(
                    subsidy_exists_region(4.7), 
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                // $get_jaw = get_jaw($tooth);
                // if(subsidy_exists(2.2) OR subsidy_exists(2.3) OR subsidy_exists(1.1))
                if(subsidy_exists(2.2) OR subsidy_exists(2.3))
                {
                    // return FALSE; // ISSUE
                }

                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-1e"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 14"]
                    );
                }
            }
            else if(  $value !== ')(' 
                AND ((right($tooth, $schema)['status'] == '' AND left($tooth, $schema)['status'] == '') OR
                (to_be_treated(right($tooth, $schema)['status']) OR right($tooth, $schema)['status'] == 'pw')) AND
                !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema)
                AND !in_array($tooth, array_merge(
                    subsidy_exists_region(4.7),
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2d"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND 
                    (!to_be_treated(right($tooth, $schema)['status'])
                        AND !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (!to_be_treated(left($tooth, $schema)['status'])
                        AND !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                if(subsidy_exists(2.2) OR subsidy_exists(2.3))
                {
                    // return FALSE;
                }
                
                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2c"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND
                    in_array($tooth, [17,27,37,47,16,46])
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                    AND
                    (//!to_be_treated(right($tooth, $schema)['status'])
                         !to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (//!to_be_treated(left($tooth, $schema)['status'])
                        to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                if(subsidy_exists(2.2) OR subsidy_exists(2.3))
                {
                    // return FALSE;
                }

                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2b"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                    AND in_array($tooth, [17,27,37,47,26,36])
                    AND
                    !in_array($tooth, array_merge(
                        subsidy_exists_region(4.7), 
                        subsidy_exists_region_2x(2.1),
                        subsidy_exists_region_2x(2.2),
                        subsidy_exists_region_2x(2.3),
                        subsidy_exists_region_2x(2.4),
                        subsidy_exists_region_2x(2.5),
                        subsidy_exists_region(2.7),
                        subsidy_exists_region(7.2)
                        ))
                    AND
                    (//!to_be_treated(right($tooth, $schema)['status'])
                         !to_be_replaced(left($tooth, $schema)['status'], $tooth, $schema))
                    AND
                     (//!to_be_treated(left($tooth, $schema)['status'])
                        to_be_replaced(right($tooth, $schema)['status'], $tooth, $schema))
                // ( to_be_treated(right($tooth, $schema)['status']) OR to_be_treated(left($tooth, $schema)['status']) )
            ) {
                if(subsidy_exists(2.2) OR subsidy_exists(2.3))
                {
                    // return FALSE;
                }

                $pos_sch = position_schema($tooth);

                if(subsidy_exists_name('StatusPwInPosteriorRegion')
                    AND in_array($TOOTH_NUMBERS_ISO[$pos_sch], subsidy_exists_region(1.2))
                ) {
                    return FALSE;
                }

                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2b"]
                );
                if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                    // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                ){
                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                    );
                }
            }
            else if( $value !== ')(' 
                AND subsidy_exists(3.1)
                AND $is_optional_3x_2x == FALSE
            ) {
                if(!in_array($tooth, array_merge(
                    subsidy_exists_region(4.7), 
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7),
                    subsidy_exists_region(7.2)
                    ))
                ) {
                    $pos_sch = position_schema($tooth);

                    array_push($teeth_subsidy_eveluate, 
                        ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2a"]
                    );
                    if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                        // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                    ){
                        array_push($teeth_subsidy_eveluate, 
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                        );
                    }
                }
            }
            else if( $value !== ')(' 
                AND $is_optional_3x_2x == TRUE
            ) {
                /*if(!in_array($tooth, array_merge(
                    subsidy_exists_region(4.7),
                    subsidy_exists_region_2x(2.1),
                    subsidy_exists_region_2x(2.2),
                    subsidy_exists_region_2x(2.3),
                    subsidy_exists_region_2x(2.4),
                    subsidy_exists_region_2x(2.5),
                    subsidy_exists_region(2.7)
                    ))
                )*/ {
                    $pos_sch = position_schema($tooth);

                    array_push($teeth_subsidy_eveluate_2x, 
                        ["subsidy"=> "1.1", "output_order"=> "16", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "ToBeTreatedWithNoAbutmentTeethIncluded-2a"]
                    );
                    if( veneering_grants($TOOTH_NUMBERS_ISO[$pos_sch]) 
                        // AND !in_array($TOOTH_NUMBERS_ISO[$pos_sch], $is_gap_closure_arr)
                    ){
                        array_push($teeth_subsidy_eveluate_2x, 
                            ["subsidy"=> "1.3", "output_order"=> "17", "region"=> $TOOTH_NUMBERS_ISO[$pos_sch], "quantity"=> "1", "applied_rule"=> "veneering_grants 15"]
                        );
                    }
                }
            }
        }
    }
}
/** 1.x end **/

/** 2.x start **/
function BiggestInterdentalGapInFrontRegionExactToBeReplaced4($schema) {
    global $teeth_inter_dental_gaps, $is_gap_closure_arr, $teeth_subsidy_eveluate, $teeth_region_eveluate, $TOOTH_NUMBERS_ISO;

    $region_arr = [];

    if(subsidy_exists(3.1) OR subsidy_exists(4)
    ) {
        return FALSE;
    }
    
    if(! is_interdental_gap($schema)) {
        return FALSE;
    }

    if(biggest_interdental_gap($teeth_inter_dental_gaps) == 4
    ) {
        foreach($teeth_inter_dental_gaps as $key => $value) {
            array_push($region_arr, $key);
            array_push($region_arr, $value);
        }

        // array_push($teeth_subsidy_eveluate, 
        //         ["subsidy"=> "2.4", "output_order"=> "10", "region"=> $region_arr[0] ."-". $region_arr[1], "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapInFrontRegionExactToBeReplaced4"]
        // );

        // for($v= position_schema($region_arr[0]); $v<= position_schema($region_arr[1]); $v++) {
        //     if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
        //         AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
        //     ) {
        //         array_push($teeth_subsidy_eveluate, 
        //             ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 14"]
        //         );
        //     }
        // }
    }
}

function BiggestInterdentalGapExactToBeReplaced3($schema) {
    global $teeth_inter_dental_gaps, $is_gap_closure_arr, $teeth_subsidy_eveluate, $teeth_region_eveluate, $TOOTH_NUMBERS_ISO;

    $region_arr = [];

    if(subsidy_exists(3.1) OR subsidy_exists(4)
    ) {
        return FALSE;
    }
    
    if(! is_interdental_gap($schema)) {
        return FALSE;
    }

    if(biggest_interdental_gap($teeth_inter_dental_gaps) == 3
    ) {
        foreach($teeth_inter_dental_gaps as $key => $value) {
            array_push($region_arr, $key);
            array_push($region_arr, $value);
        }

        array_push($teeth_subsidy_eveluate, 
                ["subsidy"=> "2.3", "output_order"=> "11", "region"=> $region_arr[0] ."-". $region_arr[1], "quantity"=> "1", "applied_rule"=> "Biggest Interdental GapExactToBeReplaced3"]
        );

        for($v= position_schema($region_arr[0]); $v<= position_schema($region_arr[1]); $v++) {
            if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
                AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
            ) {
                array_push($teeth_subsidy_eveluate, 
                    ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 15"]
                );
            }
        }
    }
}

function BiggestInterdentalGapExactToBeReplaced2($schema) {
    global $teeth_inter_dental_gaps, $is_gap_closure_arr, $teeth_subsidy_eveluate, $teeth_region_eveluate, $TOOTH_NUMBERS_ISO;

    $region_arr = [];

    if(subsidy_exists(3.1) OR subsidy_exists(4)
    ) {
        return FALSE;
    }
    
    if(! is_interdental_gap($schema)) {
        return FALSE;
    }

    // if(biggest_interdental_gap($teeth_inter_dental_gaps) == 2
    // ) {
    //     foreach($teeth_inter_dental_gaps as $key => $value) {
    //         array_push($region_arr, $key);
    //         array_push($region_arr, $value);
    //     }

    //     array_push($teeth_subsidy_eveluate, 
    //         ["subsidy"=> "2.2", "output_order"=> "12", "region"=> $region_arr[0] ."-". $region_arr[1], "quantity"=> "1", "applied_rule"=> "BiggestInterdentalGapExactToBeReplaced2 5"]
    //     );

    //     for($v= position_schema($region_arr[0]); $v<= position_schema($region_arr[1]); $v++) {
    //         if(veneering_grants($TOOTH_NUMBERS_ISO[$v])
    //             AND !in_array($TOOTH_NUMBERS_ISO[$v], $is_gap_closure_arr)
    //         ) {
    //             array_push($teeth_subsidy_eveluate, 
    //                 ["subsidy"=> "2.7", "output_order"=> "15", "region"=> $TOOTH_NUMBERS_ISO[$v], "quantity"=> "1", "applied_rule"=> "Veneering grants 2.x 16"]
    //             );
    //         }
    //     }
    // }
}

function BiggestInterdentalGapExactToBeReplaced1($schema) {
    global $teeth_inter_dental_gaps, $teeth_subsidy_eveluate, $teeth_region_eveluate;

    $region_arr = [];

    if(subsidy_exists(3.1) OR subsidy_exists(4)
    ) {
        return FALSE;
    }
    
    if(! is_interdental_gap($schema)) {
        return FALSE;
    }

    if(subsidy_exists_name('BilateralFreeEnd_upper') OR subsidy_exists(2.1)) {
        return FALSE;
    }

    // if(biggest_interdental_gap($teeth_inter_dental_gaps) == 1) {
    //    foreach($teeth_inter_dental_gaps as $key => $value) {
    //        array_push($region_arr, $key);
    //        array_push($region_arr, $value);
    //    }
    //     array_push($teeth_subsidy_eveluate, 
    //             ["subsidy"=> "2.1", "region"=> $region_arr[0] ."-". end($region_arr), "quantity"=> "1", "applied_rule"=> "Biggest InterdentalGapExactToBeReplaced1"]
    //     );
    // }
}

function gap_closure($schema) {
    global $TOOTH_NUMBERS_ISO, $is_gap_closure, $is_gap_closure_arr;
    $gap_count = 0;
    $teeth_status = [];
    $temp_schema = [];

    foreach($schema as $tooth => $status) {
        if($status !== ')(') {
            array_push($teeth_status, $status);
        }
        else {
            if(veneering_grants($tooth)
                OR
                ((right($tooth, $schema)['status'] !== '' 
                OR left($tooth, $schema)['status'] !== '')
                AND 
                !veneering_grants($tooth))
            ) {
                $is_gap_closure = 'Y';
                array_push($is_gap_closure_arr, $tooth);
            }
            else {
                array_push($teeth_status, $status);
            }
        }
    }

    //make status again 32 array
    $gap_count = 32 - count($teeth_status);
    for($i=0; $i<$gap_count; $i++) {
        array_push($teeth_status, '');
    }

    //create the new schema array
    for($i=0; $i<count($TOOTH_NUMBERS_ISO); $i++) {
        $temp_schema[$TOOTH_NUMBERS_ISO[$i]] = $teeth_status[$i];
    }

    // if(count($temp_schema) == 32) {
    //     $schema = $temp_schema;
    // }

    //return $temp_schema;
    return $schema;
}

function bs_TBR($schema)
{
    global $TOOTH_NUMBERS_ISO, $bs_separate_arr;
    $bs_array_modify = [];

    foreach($schema as $tooth => $status) {
        if ($status == 'b'
        ) {
            array_push($bs_array_modify, position_schema($tooth));
        } 
    }

    $remove_b = [];

    for($ab=0; $ab<count($bs_array_modify); $ab++) {
        if($TOOTH_NUMBERS_ISO[$bs_array_modify[$ab]] == 17
            AND !to_be_treated(get_condition(16, $schema))
            AND !to_be_replaced(get_condition(16, $schema), 16, $schema) 
            AND get_condition(16, $schema) !== 'b'
            AND to_be_replaced(get_condition(18, $schema), 18, $schema) 
        ) {
            array_push($remove_b, $ab);
        }

        if($TOOTH_NUMBERS_ISO[$bs_array_modify[$ab]] == 27
            AND !to_be_treated(get_condition(26, $schema))
            AND !to_be_replaced(get_condition(26, $schema), 26, $schema)
            AND get_condition(26, $schema) !== 'b'
            AND to_be_replaced(get_condition(18, $schema), 28, $schema)
        ) {
            array_push($remove_b, $ab);
        }

        if($TOOTH_NUMBERS_ISO[$bs_array_modify[$ab]] == 37
            AND !to_be_treated(get_condition(36, $schema))
            AND !to_be_replaced(get_condition(36, $schema), 36, $schema)
            AND get_condition(36, $schema) !== 'b'
            AND to_be_replaced(get_condition(18, $schema), 38, $schema)
        ) {
            array_push($remove_b, $ab);
        }

        if($TOOTH_NUMBERS_ISO[$bs_array_modify[$ab]] == 47
            AND !to_be_treated(get_condition(46, $schema))
            AND !to_be_replaced(get_condition(46, $schema), 46, $schema)
            AND get_condition(46, $schema) !== 'b'
            AND to_be_replaced(get_condition(48, $schema), 48, $schema)
        ) {
            array_push($remove_b, $ab);
        }
    }

    for($ab=0; $ab<count($remove_b); $ab++) {
        unset($bs_array_modify[$remove_b[$ab]]);
    }

    $bs_array_modify = array_values($bs_array_modify);

    bs_separate($bs_array_modify);

    for($bi=0; $bi<count($bs_separate_arr); $bi++) {
        // Previous of B and last ot B TBT then b are TBR
        // ww b b ww : B are TBR

        $prev_tooth = isset($TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]-1]) ?
                            $TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]-1] :
                            $TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]];

        $next_tooth = isset($TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])+1]) ? 
                            $TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])+1] : 
                            $TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])];
        
        
        if(
            (to_be_sw(get_condition($prev_tooth, $schema))
            AND
                (!to_be_treated(get_condition($next_tooth, $schema))
                    OR get_condition($next_tooth, $schema) == 'k'
                    OR get_condition($next_tooth, $schema) == 'kw'
                )
            )
            OR
            (
                (!to_be_treated(get_condition($prev_tooth, $schema))
                    OR get_condition($prev_tooth, $schema) == 'k'
                    OR get_condition($prev_tooth, $schema) == 'kw'
                )
            AND
            to_be_sw(get_condition($next_tooth, $schema)))
        ) {
            //if 4 bs then convert to TBR
            $teeth_diff = position_schema($next_tooth) - position_schema($prev_tooth);
            if($teeth_diff > 4
            ) {
                for($tf=0; $tf<count($bs_separate_arr[$bi]); $tf++ )
                {
                    $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] = 'swfb';
                }
                $schema[$prev_tooth] = 'swfb';
                $schema[$next_tooth] = 'swfb';
            }
            else {
                for($tf=0; $tf<count($bs_separate_arr[$bi]); $tf++ )
                {
                    $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] = 'swb';
                }

                if(get_condition($prev_tooth, $schema) !== ''
                    AND 
                    (get_condition($prev_tooth, $schema) == 'k'
                    OR get_condition($prev_tooth, $schema) == 'kw')
                ) {
                    $schema[$prev_tooth] = 'swk';
                }

                if(get_condition($next_tooth, $schema) !== ''
                    AND 
                    (get_condition($next_tooth, $schema) == 'k'
                    OR get_condition($next_tooth, $schema) == 'kw')
                ) {
                    $schema[$next_tooth] = 'swk';
                }
            }
        }

        if(
            (to_be_sw(get_condition($prev_tooth, $schema))
            AND
            (   to_be_treated(get_condition($next_tooth, $schema))
                AND get_condition($next_tooth, $schema) !== 'k'
                AND get_condition($next_tooth, $schema) !== 'kw'
            )
            )
            OR
            (
                (to_be_treated(get_condition($prev_tooth, $schema))
                    AND get_condition($prev_tooth, $schema) !== 'k'
                    AND get_condition($prev_tooth, $schema) !== 'kw'
                )
            AND
            to_be_sw(get_condition($next_tooth, $schema)))
        ) {
            for($tf=0; $tf<count($bs_separate_arr[$bi]); $tf++ )
            {
                $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] = 'swfb';
            }
        }

        if((
            (
                to_be_treated(get_condition($prev_tooth, $schema))
                OR to_be_replaced(get_condition($prev_tooth, $schema), $prev_tooth, $schema)
            )
            OR
            (   
                to_be_treated(get_condition($next_tooth, $schema))
                OR to_be_replaced(get_condition($next_tooth, $schema), $next_tooth, $schema)
            )
        )
        ) {
            for($tf=0; $tf<count($bs_separate_arr[$bi]); $tf++ )
            {
                if($schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] !== 'swb'
                    AND $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] !== 'swfb'
                ) {
                    $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][$tf]]] = 'f';
                }
            }
        }
        else { // ISSUES
            if($schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]]] !== 'swb'
                AND $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]]] !== 'swfb'
            ) {
                $schema[$TOOTH_NUMBERS_ISO[$bs_separate_arr[$bi][0]]] = '';
            }
            if($schema[$TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])]] !== 'swb'
                AND $schema[$TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])]] !== 'swfb'
            ) {
                $schema[$TOOTH_NUMBERS_ISO[end($bs_separate_arr[$bi])]] = '';
            }
        }
    }

    return $schema;
}

function bs_separate($bs_array_modify) {
    global $bs_separate_arr;

    $new_array = [];
        
    if(count($bs_array_modify) > 1) {
        $old_key = $bs_array_modify[0]-1;

        for($key = 0; $key < count($bs_array_modify); $key++ ) {
            $new_key = $bs_array_modify[$key];

            if(($new_key - $old_key) == 1
            ) {
                array_push($new_array, $bs_array_modify[$key]);

                $old_key++ ; // = $key;
            }
            else {
                // return $new_array;
                break;
            }
        }

        array_push($bs_separate_arr, $new_array);
        
        $result = array_values(array_diff($bs_array_modify, $new_array));
        // recursively call
        bs_separate($result);
    }
    else {
        if(count($bs_array_modify) > 0)
        {
            array_push($bs_separate_arr, $bs_array_modify);
        }
    }

    return $bs_separate_arr;
}

function sw_TBR($schema) {
    global $TOOTH_NUMBERS_ISO, $bs_separate_arr;

    foreach($schema as $tooth => $status) {
        if (to_be_replaced($status, $tooth, $schema)
            AND $status !== 'swfb'
            AND (to_be_sw(left($tooth, $schema)['status'])
                AND to_be_sw(right($tooth, $schema)['status'])
            )
        ) {
            $schema[left($tooth, $schema)['tooth']] = 'f';
            $schema[right($tooth, $schema)['tooth']] = 'f';
        }

        if(to_be_sw($status)
            AND to_be_replaced(left($tooth, $schema)['status'], left($tooth, $schema)['tooth'], $schema)
            // AND !to_be_replaced(get_condition(18, $schema), 18, $schema)
                // AND !to_be_replaced(get_condition(17, $schema), 17, $schema)
                AND left($tooth, $schema)['tooth'] !== 17
                // AND $tooth !== 16
        ) {
            $schema[$tooth] = 'f';
        }
    }

    return $schema;
}

?>
